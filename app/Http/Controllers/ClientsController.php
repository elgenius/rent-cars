<?php

namespace App\Http\Controllers;

use App\models\Rentings;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class ClientsController extends Controller
{
    public $app_url;
    public $photo_directory;
    public function __construct()
    {
        $this->app_url = url()->to('/');
        $this->photo_directory = 'clients_photos';
    }
    public $clients_fields = [
        'رقم العميل',
        'الأسم',
        'تاريخ الميلاد',
        'رقم التليفون',
        'العنوان',
        'الرقم القومى'
    ];

    // continue later in order to make a dynamic registeration fields in all  user controllers
    public $client_register_fields =
        [
            ['name' => '']
        ];

    public function GetClients()
    {
        $clients = User::select(
            'users.id',
            'users.first_name',
            'users.last_name',
            'users.birthdate',
            'users.address',
            'users.phone',
            'users.national_id',
            'roles.name')
            ->leftJoin('roles','users.role_id','=','roles.id')
            ->where('roles.name','=','Customer')
            ->get();
        return $clients;
    }

    public function GetClientsFields()
    {
        return $this->clients_fields;
    }
    public function GetClientsCounter()
    {
        $user = DB::select('select count(*) as counter from users where role_id = 4');
        return $user[0]->counter;
    }
    function RemoveFolder($dir)
    {
        foreach(scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
            else unlink("$dir/$file");
        }
        rmdir($dir);
    }
    public function DeleteClient(Request $request)
    {
        $data = $request;
        $user = User::where('id', $data['id'])->first();
        $rentings = Rentings::where('user_id',$data['id'])->withTrashed()->get();
        if($user->renting)
        {
            foreach ($rentings as $renting)
            {
                $renting->user_id = NULL;
                $renting->update();
            }
        }
        if($user->attachments)
        {
            foreach ($user->attachments as $attachment)
            {
                $attachment->user_id = NULL;
                $attachment->forcedelete();
            }
        }
        $upload_to = app_path() . '/../public/'.$this->photo_directory."/".$user->id;
        $this->RemoveFolder($upload_to);
        $user->forcedelete();

    }
    public function Profile()
    {
        $id = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '-')+1);
        $user = User::find($id);
        if($user)
        {
            $rentings = Rentings::where("user_id",$id)->withTrashed()->get();
            $dept = 0;
            $userate = 0;
            $payrate=0;
            if($rentings->count())
            {

                foreach ($rentings as $renting)
                {
                    $dept += $renting->dept;
                    if($renting->userate>0)
                        $userate+=$renting->userate;
                    if($renting->payrate>0)
                        $payrate+=$renting->payrate;
                }
                $userate = $userate/$rentings->count();
                $payrate = $payrate/$rentings->count();
            }
            return view('pages.ClientProfile',
                [
                    'user' =>$user,
                    'rentings' =>$rentings,
                    'dept'=>$dept,
                    'userate'=>$userate,
                    'payrate'=>$payrate
                ]);
        }
        else
            return view('errors.404');

    }

    /**
     * @return array
     */
    public function UpdateClient(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'birthdate' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|numeric',
            'national_id' => 'required|numeric',
            'email' => 'required|max:255',
        ]);
        $user = User::find($request->id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->birthdate = $request->birthdate;
        $user->phone = $request->phone;
        $user->national_id = $request->national_id;
        $user->address = $request->address;
        $user->update();
    }
    public function AddClientImage(Request $request , $file)
    {
        $success = 0;
        $file_name = '';
        if($request->picture) {

            $logo = $request->file('picture');
            $upload_to = app_path() . '/../public/'.$this->photo_directory."/".$request->id;
            $extension = $logo->getClientOriginalExtension();
            $file_name = $file.".$extension";
            $success = $logo->move($upload_to, $file_name);
        }
        if ($success)
            return $this->app_url."/".$this->photo_directory."/".$request->id."/".$file_name;
        else
            return $success;
    }
    public function PayDept(Request $request)
    {
        $this->validate($request,[
            'dept' => 'required|numeric',
        ]);
        $renting = Rentings::withTrashed()->find($request->id);
        if($request->dept<=$renting->dept &&$request->dept>0)
        {
            $renting->payed += $request->dept;
            $renting->update();
        }
        else
            return 2;

    }

}
