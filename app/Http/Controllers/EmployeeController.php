<?php

namespace App\Http\Controllers;

use App\models\Employees;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class EmployeeController extends Controller
{

    public $employee_fields = [
        'رقم الموظف',
        'الأسم',
        'تاريخ الميلاد',
        'رقم التليفون',
        'العنوان',
        'الرقم القومى',
        'المرتب',
    ];

    public function GetEmployees()
    {
        $employees = Employees::select(
            'employees.id',
            'employees.salary',
            'employees.user_id',
            'users.first_name',
            'users.last_name',
            'users.birthdate',
            'users.address',
            'users.phone',
            'users.national_id')
            ->leftJoin('users','employees.user_id','=','users.id')
            ->get();
        return $employees;
    }

    public function GetEmployeeFields()
    {
        return $this->employee_fields;
    }
    public function Update(Request $request)
    {

        $this->validate($request,[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'birthdate' => 'required|max:255',
            'phone' => 'required|numeric',
            'salary' => 'required|numeric',
            'national_id' => 'required|numeric',
            'address' => 'required'

        ]);
        $user = User::find($request->user_id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->birthdate = $request->birthdate;
        $user->phone = $request->phone;
        $user->national_id = $request->national_id;
        $user->address = $request->address;
        $user->update();
        $employee = Employees::find($request->id);
        $employee->salary = $request->salary;
        $employee->save();

    }
}
