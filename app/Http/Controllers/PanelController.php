<?php

namespace App\Http\Controllers;

use App\models\Roles;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;


class PanelController extends Controller
{

    public $user;
    public $role;
    public $emp;
    public $clients;
    public $partners;
    public $cars;
    public $reservation;
    public $expenses;
    public function __construct()
    {
        $this->user = Auth::user();
        $this->role = Roles::class;
        $this->emp = new EmployeeController();
        $this->clients = new ClientsController();
        $this->partners = new PartnersController();
        $this->cars = new CarsController();
        $this->reservation = new ReservationController();
        $this->expenses = new ExpensesController();
    }

    public function OverView()
    {
        return view('pages.overview',['user' => $this->user,'cars' => $this->cars->GetCars(),'rentings' => $this->reservation->GetRentings(), 'role' => $this->role , 'page' => 'overview']);
    }

    public function Clients()
    {

        return view('pages.clients',
            [
                'user' => $this->user,
                'role' => $this->role ,
                'clients' => $this->clients->GetClients(),
                'clients_fields' => $this->clients->GetClientsFields(),
                'clients_counter' => 1,
                'page' => 'client'
            ]);
    }

    public function employees()
    {
        return view('pages.employees',
            [
                'user' => $this->user,
                'role' => $this->role ,
                'employees' => $this->emp->GetEmployees(),
                'employee_fields' =>  $this->emp->GetEmployeeFields(),
                'page' => 'employee'
            ]);
    }

    public function partners()
    {
      return view('pages.partners',
          [
              'user' => $this->user,
              'role' => $this->role ,
              'partners' => $this->partners->GetPartners(),
              'partners_fields' => $this->partners->GetPartnersFields(),
              'page' => 'partner'
          ]);
    }

    public function cars()
    {
        return view('pages.cars',
            [
                'user' => $this->user,
                'role' => $this->role ,
                'page' => 'cars',
                'cars_fields' => $this->cars->cars_fields,
                'cars_register_fields' => $this->cars->cars_register_fields,
                'cars' => $this->cars->GetCars(),
            ]);
    }

    public function reservations()
    {
        return view('pages.reservation',
            [
                'user' => $this->user,
                'role' => $this->role ,
                'page' => 'reservations',
                'users' => $this->reservation->GetCustomers(),
                'cars' => $this->reservation->GetCars(),
                'rentings_fields' => $this->reservation->GetRentingsFields(),
                'rentings' => $this->reservation->GetRentings(),

            ]);
    }
    public function recive()
    {
        return view('pages.recive',
            [
                'user' => $this->user,
                'role' => $this->role ,
                'page' => 'recive',
                'users' => $this->reservation->GetCustomers(),
                'cars' => $this->reservation->GetCars(),
                'rentings' => $this->reservation->GetRentings()
            ]);
    }

    public function reports()
    {
        return view('pages.reports',['user' => $this->user, 'role' => $this->role ,'page' => 'reports']);
    }
       public function expenses()
    {
        return view('pages.expenses',
            [
                'user' => $this->user,
                'role' => $this->role ,
                'expenses_fields' => $this->expenses->GetExpensesFields(),
                'general_expenses' => $this->expenses->GetGeneralExpenses(),
                'page' => 'expenses'
            ]);
    }

    public function quick_access()
    {

        return view('pages.quickaccess',['user' => $this->user,
            'users' => $this->reservation->GetCustomers(),
            'cars' => $this->reservation->GetCars(),
            'role' => $this->role ,'page' => 'quickaccess']);
    }

}
