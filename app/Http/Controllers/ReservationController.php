<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestReservation;
use App\Http\Utils\Utils;
use App\models\Attachments;
use App\models\Cars;
use App\models\Rentings;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{

    public $rent_fields =
        [
            'رقم الحجز',
            'اسم العميل',
            'اسم السيارة',
            'من',
            'الى',
            'المبلغ المطلوب',
            'المبلغ المدفوع',
            'المبلغ المتبقى',
        ];
    public $utils;
    public $cars;
    public function __construct()
    {
        $this->utils = new Utils();
        $this->cars = new CarsController();
    }
    public function GetRentingsFields()
    {
        return $this->rent_fields;
    }

    public function GetCustomers()
    {
        $user = User::select(
            'users.id',
            'users.first_name',
            'users.last_name',
            'users.phone'
        )
            ->where('role_id',4)
            ->get();
        return $user;
    }

    public function GetRentings()
    {
        return $rentings = Rentings::orderBy("id","desc")->withTrashed()->get();
    }

    public function GetCars()
    {
        return Cars::where('available',1)->get();
    }

    public function CalculateReservation(Request $request)
    {
        $data = $request->except('_token');
        $car = Cars::where('id',$data['car_id'])->first();
        $days = $this->utils->DateDiff($data['start_duration'],$data['end_duration'],'DAY');
        $total_price = (int)$car->price * (int)$days;
        if($data['DiscountOption'])
            $total_price = $total_price - $data['discount'];

        return $total_price;

    }
    public function CreateReservation(RequestReservation $request)
    {
        $data = $request->except(['_token','DiscountOption']);
        /**
         * additional validations
         * difference between start and end durations
         */
        $request->session()->flash('data', $data);
        return url('/checkout');
    }
    public function  ReservationCheckOut(Request $request)
    {
        $data = $request->session()->get('data');
        if(isset($data)) {

            $today = $this->utils->GetDateToday();
            $user = User::where('id', $data['user_id'])->first();
            $car_user_data = Cars::select(
                'cars.*',
                'users.*'
            )
                ->leftJoin('users', 'cars.user_id', '=', 'users.id')
                ->where('cars.id', '=', $data['car_id'])
                ->first();

            $days = $this->utils->DateDiff($data['start_duration'], $data['end_duration'], 'DAY');
            $request->session()->flash('dataP2', $data);
            $request->session()->flash('dataP3', $car_user_data);
            $request->session()->flash('dataP4',$days);

            return view('pages.checkout',
                [
                    'data' => $data,
                    'today' => $today,
                    'car_user_data' => $car_user_data,
                    'renter' => $user,
                    'day_name' => $this->utils->GetDayName(),
                    'number_of_days' => $days
                ]);
        }else{
            return view('errors.403');
        }
    }

    public function FinalReservationStep(Request $request)
    {
        $data = $request->session()->get('dataP2');
        $carUserData = $request->session()->get('dataP3');
        $days = $request->session()->get('dataP4');

        if(isset($data) && isset($carUserData) && isset($days) ) {
            $data['total'] = $carUserData->price * $days;
            $this->ChangeCarAvailbleState($data['car_id']); // change car available state


            $rental = Rentings::create($data);
            if ($rental)
                return 1;
            else
                return 0;
        }else{
            return -1;
        }
    }

    public function ChangeCarAvailbleState($car_id)
    {
        $car = Cars::where('id',$car_id)->first();
        $car->available = (int)!$car->available;
        $car->update();
    }
    public function Delete(Request $request)
    {
        $renting = Rentings::find($request->id);
        $renting->car->available = 1;
        $renting->car->update();
        $renting->delete();
    }
    public function Recive(Request $request)
    {
        $this->validate($request,[
        'KMCounter' => 'required|numeric',
        'userate' => 'required|numeric|max:10',
        'payrate' => 'required|numeric|max:10',
    ]);

        $renting = Rentings::find($request->id);
        $attachment = new Attachments();
        if($renting->deleted_at==NULL)
        {
            $renting->car->KMCounter=$request->KMCounter;
            $renting->notes = $request->notes;
            $renting->userate = $request->userate;
            $renting->payrate = $request->payrate;
            $this->ChangeCarAvailbleState($renting->car->id);
            $attachment->cars_id = $renting->car->id;
            $attachment->title="استلام - ".$renting->end_duration;
            $request->picture = $this->cars->AddCarImage($request,$renting->end_duration);
            $attachment->value=$request->picture;
            $attachment->save();
            $renting->update();
            $renting->car->update();
            $renting->delete();

        }
    }
}
