<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestCar;
use App\models\Cars;
use App\models\RentalType;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class CarsController extends Controller
{
    public $expenses;
    public $cars_fields =
        [
            'الصورة',
            'النوع',
            'الموديل',
            'اللون',
            'رقم اللوحة',
            'رقم الموتور',
            'رقم الشاسيه',
            'رقم الرخصة',
            'اسم المالك',
            'السعر',
        ];

    public $cars_register_fields =
        [
            ['name' => 'type' , 'placeholder' => 'الأسم' , 'type' => 'text'],
            ['name' => 'model' , 'placeholder' => 'الموديل' , 'type' => 'text'],
            ['name' => 'color' , 'placeholder' => 'اللون' , 'type' => 'text'],
            ['name' => 'license_number' , 'placeholder' => ' رقم الرخصة' , 'type' => 'text'],
            ['name' => 'plate_number' , 'placeholder' => 'رقم اللوحة' , 'type' => 'text'],
            ['name' => 'motor_number' , 'placeholder' => 'رقم الموتور' , 'type' => 'text'],
            ['name' => 'chassis_number' , 'placeholder' => 'رقم الشاسيه' , 'type' => 'text'],
            ['name' => 'price' , 'placeholder' => 'السعر في اليوم' , 'type' => 'text'],
            ['name' => 'user_id' , 'placeholder' => 'المالك' , 'type' => 'select' , 'options' => [] ],
            ['name' => 'rental_type_id' , 'placeholder' => 'نوع الأيجار' ,'type' => 'select' , 'options' => [] ],
            ['name' => 'renter_commission' , 'placeholder' => 'العمولة' , 'type' => 'text'],
        ];

    public $app_url;
    public $photo_directory;
    public function __construct()
    {
        $this->app_url = url()->to('/');
        $this->photo_directory = 'cars_photos';
        $this->fillRentalTypeOptions();
        $this->fillPartnerOptions();
        $this->expenses = new ExpensesController();
    }

    public function RegisterCar(RequestCar $request)
    {
        $data = $request->except('_token');
        $data['picture']  = $this->AddCarImage($request,"main");
        $car = Cars::create($data);
        if($car)
            return 1;
        else
            return 0;
    }

    public function AddCarImage(Request $request , $file)
    {
        $success = 0;
        $file_name = '';
        if($request->picture) {

            $logo = $request->file('picture');
            $upload_to = app_path() . '/../public/'.$this->photo_directory."/".$request->plate_number;
            $extension = $logo->getClientOriginalExtension();
            $file_name = $file.".$extension";
            $success = $logo->move($upload_to, $file_name);
        }
        if ($success)
            return $this->app_url."/".$this->photo_directory."/".$request->plate_number."/".$file_name;
        else
            return $success;
    }

    public function GetCars()
    {
        $cars = Cars::select(
            'cars.*',
            'users.first_name',
            'users.last_name')
            ->leftJoin('users','cars.user_id','=','users.id')
            ->get();
        return $cars;
    }

    public function fillRentalTypeOptions()
    {
        $rentals = RentalType::select(
            'rental_types.id as value',
            'rental_types.name as display_name'
        )->where('id','>',0)
            ->get();
        for ($i = 0; $i < sizeof($this->cars_register_fields) ; $i++)
        {
            if($this->cars_register_fields[$i]['name'] == "rental_type_id")
            {
                $this->cars_register_fields[$i]['options'] = $rentals;
            }
        }
    }

    public function fillPartnerOptions()
    {
        $partners = User::select(
            'users.first_name',
            'users.last_name',
            'users.id as value'
        )
            ->where('role_id',2)
            ->get();
        for ($i = 0; $i < sizeof($this->cars_register_fields) ; $i++)
        {
            if($this->cars_register_fields[$i]['name'] == "user_id")
            {
                $this->cars_register_fields[$i]['options'] = $partners;
            }
        }
    }
    function RemoveFolder($dir)
    {
        foreach(scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
            else unlink("$dir/$file");
        }
        rmdir($dir);
    }
    public function Delete(Request $request)
    {
        $data = $request;
        $car = Cars::where('id',$data['id'])->first();
        $upload_to = app_path() . '/../public/'.$this->photo_directory."/".$car->plate_number;
        $this-> RemoveFolder($upload_to);
        $car->delete();
    }
    public function Profile()
    {
        $id = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '-')+1);
        $car = Cars::find($id);
        if($car)
        {
            $rentings = $car->rentings;
            return view('pages.CarProfile',
                [
                    'car' =>$car,
                    'rentings' =>$rentings,
                    'expenses_fields' => $this->expenses->GetExpensesFields(),
                    'car_expenses' => $this->expenses->GetCarExpenses($id),
                ]);
        }
        else
            return view('errors.404');

    }
    public function UpdateCar(Request $request)
    {
        $this->validate($request,[
            'type' => 'required|max:255',
            'model' => 'required|numeric',
            'color' => 'required|max:255',
            'license_number' => 'required|numeric',
            'plate_number' => 'required',
            'motor_number' => 'required|numeric',
            'chassis_number' => 'required|numeric',
             'price' => 'required|numeric',
        
        ]);
        $car = Cars::find($request->id);
        $car->type = $request->type;
        $car->model = $request->model;
        $car->color = $request->color;
        $car->license_number = $request->license_number;
        $car->plate_number = $request->plate_number;
        $car->motor_number = $request->motor_number;
        $car->chassis_number = $request->chassis_number;
        $car->price = $request->price;
        $car->update();
    }
}