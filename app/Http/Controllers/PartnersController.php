<?php

namespace App\Http\Controllers;

use App\models\Cars;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class PartnersController extends Controller
{
    public $partners_fields = [
        'رقم الشريك',
        'الأسم',
        'تاريخ الميلاد',
        'رقم التليفون',
        'العنوان',
        'الرقم القومى'
    ];

    public function GetPartners()
    {
        $partners = User::select(
            'users.id',
            'users.first_name',
            'users.last_name',
            'users.birthdate',
            'users.address',
            'users.phone',
            'users.national_id',
            'roles.name')
            ->leftJoin('roles','users.role_id','=','roles.id')
            ->where('roles.name','=','Partner')
            ->get();
        return $partners;
    }

    public function GetPartnersFields()
    {
        return $this->partners_fields;
    }
    public function DeletePartner(Request $request)
    {
        $data = $request;
        $user = User::where('id', $data['id'])->first();
        $cars = Cars::where('user_id',$data['id'])->get();
        if($cars)
        {
                foreach($cars as $car)
                {
                    $car->user_id = NULL;
                    $car->update();
                    $car->delete();
                }
            $user->forcedelete();
        }
        else
            $user->forcedelete();

    }


}
