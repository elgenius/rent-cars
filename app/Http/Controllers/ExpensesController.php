<?php

namespace App\Http\Controllers;

use App\models\Expenses;

use Illuminate\Http\Request;

use App\Http\Requests;

class ExpensesController extends Controller
{

    public $ExpensesFields = [
        'العنوان',
        'القيمة',
    ];
    public function GetExpensesFields()
    {
        return $this->ExpensesFields;
    }
    public function GetGeneralExpenses()
   {
        $general_expenses = Expenses::where("type","1")->get();
        return $general_expenses;
   }
    public function GetCarExpenses($id)
    {
        $general_expenses = Expenses::where("car_id",$id)->get();
        return $general_expenses;
    }
    public function AddGeneral(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'value' => 'required|numeric',
        ]);
        $expense = new Expenses();
        $expense->title=$request->title;
        $expense->value=$request->value;
        $expense->type=1;
        $expense->save();

    }
    public function Delete(Request $request)
    {
        $data = $request;
        $expense = Expenses::where('id',$data['id'])->first();
        $expense->forcedelete();
    }
    public function Update(Request $request)
    {
        $expense = Expenses::find($request->id);
        $expense->title = $request->title;
        $expense->value = $request->value;
        $expense->update();
    }
    public function AddCar(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'value' => 'required|numeric',
        ]);
        $expense = new Expenses();
        $expense->car_id=$request->car_id;
        $expense->title=$request->title;
        $expense->value=$request->value;
        $expense->type=2;
        $expense->save();
    }

}
