<?php

namespace App\Http\Controllers;

use App\models\Roles;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\models\Attachments;



class AttachmentsController extends Controller
{

    public $user;
    public $clients;
    public $cars;
    public $reservation;
    public function __construct()
    {
        $this->user = Auth::user();
        $this->clients = new ClientsController();
        $this->cars = new CarsController();
        $this->reservation = new ReservationController();
    }
    public function Car(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
        ]);
        $attachment = new Attachments();
        $attachment->save();

        $attachment->cars_id = $request->id;
        $attachment->title=$request->title;
        $request->picture = $this->cars->AddCarImage($request,$request->title."-".$attachment->id);
        $attachment->value=$request->picture;
        $attachment->update();
    }
    public function Client(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
        ]);
        $attachment = new Attachments();
        $attachment->save();
        $attachment->user_id = $request->id;
        $attachment->title=$request->title;
        $request->picture = $this->clients->AddClientImage($request,$request->title."-".$attachment->id);
        $attachment->value=$request->picture;
        $attachment->update();
    }

}
