<?php
use Illuminate\Support\Facades\DB;
Route::get('trial',function (){
});
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;


Route::get('/','RouteController@index');
Route::get('home','RouteController@index');
Route::get('/password/email','Auth\PasswordController@getEmail');
Route::post('/password/email','Auth\PasswordController@postEmail');
Route::get('/password/reset/{token}','Auth\PasswordController@getReset');
Route::post('/password/reset','Auth\PasswordController@getReset');


Route::post('/user/login','HomeController@PostLogin');
Route::post('/user/register/{role}','HomeController@RegisterUsers')->middleware(['RegisterPermitter']);
Route::post('/contact','HomeController@Contact');


Route::group(['middleware' => 'auth'],function (){
    Route::get('/logout',array('as' => 'logout' ,'uses' => 'HomeController@LogOut'));
    Route::get('/dashboard',array('as' => 'dashboard' , 'uses' => 'RouteController@Dashboard') );

    Route::get('/overview',array('as' => 'overview' , 'uses' => 'PanelController@OverView' ))->middleware(['RoleChecker:overview']);
    Route::get('/clients', array('as' => 'clients', 'uses' => 'PanelController@Clients'))->middleware(['RoleChecker:clients']);
    Route::get('/employees', array('as' => 'employees', 'uses' => 'PanelController@employees'))->middleware(['RoleChecker:employees']);
    Route::get('/partners', array('as' => 'partners', 'uses' => 'PanelController@partners'))->middleware(['RoleChecker:partners']);
    Route::get('/cars', array('as' => 'cars', 'uses' => 'PanelController@cars'))->middleware(['RoleChecker:cars']);
    Route::get('/reservations', array('as' => 'reservations', 'uses' => 'PanelController@reservations'))->middleware(['RoleChecker:reservations']);
    Route::get('/recive', array('as' => 'recive', 'uses' => 'PanelController@recive'))->middleware(['RoleChecker:recive']);
    Route::get('/expenses',array('as' => 'expenses' , 'uses' => 'PanelController@expenses' ))->middleware(['RoleChecker:expenses']);
    Route::get('/reports', array('as' => 'reports', 'uses' => 'PanelController@reports'))->middleware(['RoleChecker:reports']);
    Route::get('/quickaccess', array('as' => 'quickaccess', 'uses' => 'PanelController@quick_access'))->middleware(['RoleChecker:quickaccess']);
    Route::get('/checkout',array('as'=>'checkout','uses'=>'ReservationController@ReservationCheckOut'));
    Route::post('/register/car',array('as' => 'register.car', 'uses' => 'CarsController@RegisterCar'))->middleware(['RoleChecker:cars']);
    Route::post('/client/delete','ClientsController@DeleteClient');
    Route::post('/partner/delete','PartnersController@DeletePartner');
    Route::post('/rent/car',array('as' => 'rent.car','uses' => 'ReservationController@CreateReservation'));
    Route::post('/calculate/reservation',array('as' => 'calculate.car.rent','uses' => 'ReservationController@CalculateReservation'));
    Route::post('/Reserve',array('as'=> '','uses' => 'ReservationController@FinalReservationStep'));
    Route::post('/reservation/delete',array('as'=> '','uses' => 'ReservationController@Delete'));
    Route::post('/renting/recive','ReservationController@Recive');
    Route::post('/employee/update','EmployeeController@Update');

    Route::get('user/{username}','ClientsController@Profile');
    Route::get('car/{carname}','CarsController@Profile');

    Route::post('/user/update','HomeController@UpdateUser');
    Route::post('/client/update','ClientsController@UpdateClient');
    Route::post('/client/dept/pay','ClientsController@PayDept');

    Route::post('/expenses/general/add','ExpensesController@AddGeneral');
    Route::post('/expenses/car/add','ExpensesController@AddCar');
    Route::post('/expenses/delete','ExpensesController@Delete');
    Route::post('/expenses/update','ExpensesController@Update');


    Route::post('/car/delete','CarsController@Delete');
    Route::post('/car/update','CarsController@Update');
    Route::post('/car/attachment','AttachmentsController@Car');
    Route::post('/client/attachment','AttachmentsController@Client');


});
