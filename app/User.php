<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name','last_name', 'birthdate', 'address','password',
        'email','phone','national_id','role_id'];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\models\Roles');
    }

    public function cars()
    {
        return $this->hasMany('App\models\Cars');
    }

    public function employee()
    {
        return $this->hasOne('App\models\Employees');
    }

    public function renting()
    {
        return $this->hasMany('App\models\Rentings');
    }
    public function attachments()
    {
        return $this->hasMany('App\models\Attachments');
    }
    public function getDisplayNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getBirthAttribute()
    {
        $birthdate = explode(" ",$this->birthdate);
        return $birth = $birthdate[0];
    }

}
