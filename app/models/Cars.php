<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cars extends Model
{
    use SoftDeletes;
    protected $table = 'cars';
    protected $fillable = ['type', 'picture','model','color','license_number','motor_number','chassis_number','plate_number',
        'user_id','price','rental_type_id','renter_commission','condition','expenses','available'];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function attachments()
    {
        return $this->hasMany('App\models\Attachments');
    }
    public function getDisplayNameAttribute()
    {
        return $this->type." ".$this->color." ".$this->plate_number;
    }
}