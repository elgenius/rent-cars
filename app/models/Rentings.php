<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rentings extends Model
{
    use SoftDeletes;
    protected $table = 'rentings';
    protected $fillable = ['user_id', 'car_id','start_duration','end_duration','payed','total','discount','rate'];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function car()
    {
        return $this->belongsTo('App\models\Cars');
    }

    public function getDeptAttribute()
    {
        $dept = $this->total - ($this->discount + $this->payed);
        return $dept;
    }
}