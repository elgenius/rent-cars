# Renting Cars Website Development

[![Gitter](https://badges.gitter.im/abdoolly/Rent-Cars-Website.svg)](https://gitter.im/abdoolly/Rent-Cars-Website?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)


## Instructions

- install any git manager
- pull the project to your pc
- create a database with name "car_renter"

# make the following commands

- php artisan migrate
- php artisan db:seed

Now you have created the database tables and seeded the user roles.

## APTWARE Development Company