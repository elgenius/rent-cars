<?php

use App\Http\Utils\Permission;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{

    protected $permissions;
    public function __construct()
    {
        $this->permissions = new Permission();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\models\Roles::create([
            'name' => "Admin",
            'description'=> "User that have all permissions in the system",
            'permissions' => json_encode(['overview','clients','employees','partners','cars','reservations','reports','Partner','Employee','Customer'])
        ]);

        \App\models\Roles::create([
            'name' => "Partner",
            'description'=> "User that have a car that he wants to rent it",
            'permissions' => json_encode(['overview','cars','reservations'])
        ]);

        \App\models\Roles::create([
            'name' => "Employee",
            'description'=> "User that handle rents and invoices in the system",
            'permissions' => json_encode(['quickaccess','clients','reservations','Customer'])
        ]);

        \App\models\Roles::create([
            'name' => "Customer",
            'description'=> "User that rents the car in the system",
            'permissions' => ""
        ]);
    }
}
