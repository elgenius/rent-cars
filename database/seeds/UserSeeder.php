<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->first_name = "abdallah";
        $user->last_name = "gamal";
        $user->birthdate = "1996-07-07";
        $user->address = "ain shams";
        $user->password  = bcrypt("0146522653");
        $user->email = "abdoolly@gmail.com";
        $user->phone = "01210073443";
        $user->national_id = "45454654654";
        $user->role_id = "1";
        $user->save();

        $user->first_name = "Admin";
        $user->last_name = "";
        $user->birthdate = "1996-07-07";
        $user->address = "ain shams";
        $user->password  = bcrypt("123");
        $user->email = "admin@first-car.com";
        $user->phone = "01210073443";
        $user->national_id = "26711010201235";
        $user->role_id = "1";
        $user->save();

    }
}
