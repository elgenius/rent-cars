<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class Rentings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('car_id')->unsigned();
            $table->timestamp('start_duration');
            $table->timestamp('end_duration');
            $table->decimal('payed',10,2)->default(0);
            $table->decimal('total',10,2);
            $table->decimal('discount',10,2)->nullable();
            $table->integer('rate')->nullable(); // user evaluation value scale from 1 to 10
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('rentings', function(Blueprint  $table)
        {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('car_id')->references('id')->on('cars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rentings');
    }
}
