<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Expenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id')->unsigned()->nullable();
            $table->string('title');
            $table->double('value');
            $table->tinyInteger('type');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('expenses', function(Blueprint  $table)
        {
            $table->foreign('car_id')->nullable()->references('id')->on('cars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
