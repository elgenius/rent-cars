<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Attachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cars_id')->unsigned()->nullable();
            $table->integer('users_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('value');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('attachments', function(Blueprint  $table)
        {
            $table->foreign('cars_id')->nullable()->references('id')->on('cars');
            $table->foreign('users_id')->nullable()->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
