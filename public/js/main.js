$(document).ready(function() {
    var current;
    $("#withdiscount").click(function () {
        $("#discountinput").fadeIn(400);
        current = parseInt($("#RequireMoney").text());

    })
    $("#withoutdiscount").click(function () {
        $("#discountinput").fadeOut(400);
    })
    $("#discountinput input").keyup(function () {
        var discount = parseInt($(this).val());
        if (discount < current && discount != "") {
            var total = current - discount;
            $("#RequireMoney").text(total);
        }
        else
            $("#RequireMoney").text(current);
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    var DocumentWidth = $(window).width();
    $("section.content").width(DocumentWidth - 250)
    $("html").niceScroll({
        cursorcolor: "#032237",
        cursorborder: "1px solid #c4c3c3",
        cursoropacitymin: "1",
        zindex: "9999",
        mousescrollstep: "60"

    })


    $("header i.fa-bars").click(function () {
        $(this).hide();
        $("i.fa-close").fadeIn();
        $(".SideBar").show();
        var ContentWidth = $(".content").width();
        var value = 50;
        $(".SideBar").width(value);
        $("#wrap").css("marginRight", value)
    });
    $("header i.fa-close").click(function () {
        $(this).hide();
        $("i.fa-bars").fadeIn();
        var ContentWidth = $(".content").width();
        $(".SideBar").animate({width: '0'}, 400, function () {
            $(".SideBar").delay(700).fadeOut(1);
        });
        $("#wrap").animate({marginRight: '0'}, 400);

    });
    $(".background , .popup i.fa-close").click(function () {

        $(".popup , .popup2").fadeOut(300, function () {
            $(".background").fadeOut(300);
        })
    });


    $("*").click(function () {
        if (this.hasAttribute('data-popup')) {
            var popup = $(this).attr("data-popup");
            $(".popup").fadeOut(300, function () {
                $(".background").fadeIn(300, function () {
                    $("#" + popup).fadeIn(300);
                })
            })
        }
        else {
        }

    })
    $("#Expenses .options button").click(function () {
        var SectionID = $(this).attr('data-SectionID');
        $("#Expenses .options").fadeOut(500, function () {
            $(SectionID).fadeIn(500);
        })
    })
    $("#Expenses #back").click(function () {
        var SectionID = $(this).attr('data-SectionID');
        $(SectionID).fadeOut(500, function () {
            $("#Expenses .options").fadeIn(500);
        })
    })

});
