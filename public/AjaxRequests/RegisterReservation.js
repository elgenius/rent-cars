$(function () {

    var discount_option = null;

    $('#RequireMoney').html("0.00");

    function GetDataToSend() {
        return {
            _token: $('input[name="_token"]').val(),
            car_id: $('select[name="car_id"]').val(),
            start_duration: $('input[name="start_duration"]').val(),
            end_duration: $('input[name="end_duration"]').val(),
            DiscountOption: discount_option,
            discount: $('input[name="discount"]').val()
        };
    }

    function SendCalculationInfo() {
        $.ajax({
            url: '/calculate/reservation',
            type: 'POST',
            data: GetDataToSend(),
            success: function (data) {
                if (data) {
                    $('#RequireMoney').html(data);
                    $('input[name="reservation_required_money"]').val(data);
                }
            },
            error: function (data) {
                alert(data['responseText']);
                var error = data.responseJSON;
            }
        });
    }

    $('input[name="DiscountOption"]').change(function () {
        discount_option = this.value;
        if (this.value == 0) {
            SendCalculationInfo();
        }
    });

    $('input[name="discount"]').change(function () {
        SendCalculationInfo();
    });

    $('input[name="car_id"]').change(function () {
        SendCalculationInfo();
    });

    $('input[name="start_duration"]').change(function () {
        SendCalculationInfo();
    });

    $('input[name="end_duration"]').change(function () {
        SendCalculationInfo();
    });


    $('#AddReservation').submit(function (e) {
        e.preventDefault();
        var button = $("#AddReservation button[type='submit']");
        button_waiting(button);
        $.ajax({
            url: '/rent/car',
            type: 'POST',
            data: $('#AddReservation').serialize(),
            success: function (data) {
                alert(data);
                button_done(button,"اضافة حجز");
                empty_errors([
                    '#reservation_user_id',
                    '#reservation_car_id',
                    '#reservation_start_duration',
                    '#reservation_end_duration',
                    '#reservation_DiscountOption',
                    '#reservation_discount',
                    '#reservation_payed'
                ]);
                if(data)
                {
                    location.href = data;
                }
            },
            error: function (data) {
                alert(data);
                button_done(button,"اضافة حجز");
                alert(data['responseText']);
                var error = data.responseJSON;
                error_handler(
                    error,
                    [
                        '#reservation_user_id',
                        '#reservation_car_id',
                        '#reservation_start_duration',
                        '#reservation_end_duration',
                        '#reservation_DiscountOption',
                        '#reservation_discount',
                        '#reservation_payed'
                    ],
                    [
                        'user_id',
                        'car_id',
                        'start_duration',
                        'end_duration',
                        'DiscountOption',
                        'discount',
                        'payed'
                    ]
                );
            }
        });
    });

    $('#End_Print').click(function () {
        var contract;
        $.ajax({
            url: '/Reserve',
            type: 'POST',
            data: {
                _token: $('input[name="_token"]').val()
            },
            success:function (data) {
                alert(data);
                if(data == -1)
                {
                    // session expired
                    HideItems();
                    print();
                    ShowItems();
                }else if (data == 0)
                {
                    // something went wrong
                }else if(data == 1)
                {
                    HideItems();
                    print();
                    ShowItems();
                }
                $("#End_Print2").fadeIn();
            },
            error:function (data) {
                // alert(data['responseText']);
            }
        });
    });
    $('#End_Print2').click(function () {
                    HideItems2();
                    print();
                    ShowItems2();

        });
    function HideItems() {
        HideElement('#End_Print');
        HideElement('#End_Print2');
        HideElement('#footer');
        document.title = "";
    }
    
    function ShowItems() {
        ShowElement('#End_Print');
        ShowElement('#End_Print2');
        ShowElement('#footer');
        document.title = "تأكيد الحجز";
    }
    function HideItems2() {
        ShowElement('.approval');
        HideElement('#End_Print');
        HideElement('#End_Print2');
        HideElement('#contractor');
        HideElement('#footer');
        document.title = "";
    }

    function ShowItems2() {
        HideElement('.approval');
        ShowElement('#End_Print');
        ShowElement('#End_Print2');
        ShowElement('#contractor');
        ShowElement('#footer');
        document.title = "تأكيد الحجز";
    }
});