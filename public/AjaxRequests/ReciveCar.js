$(document).ready(function(){
  var UserID;
  $(".item").click(function(){
    if(this.hasAttribute('data-id'))
    {
       UserID = $(this).attr('data-id');
        alert(UserID);
      $("#ReciveCar-Popup #IDVal").val(UserID);
    }
  });
  $("#ReciveCar-Popup form").submit(function(e){
  var button = $("#ReciveCar-Popup form button");
    button_waiting(button);
    e.preventDefault();
    $.ajax({
      url:"/renting/recive",
      type: 'POST',
      data : new FormData(this),
      contentType: false,
      cache      : false,
      processData: false,
      success:function(data){
        button_done(button,"استلام");
        PrintOnSelector("#ReciveCar-Popup form .alert","تم الاستلام بنجاح بنجاح");
        $("#ReciveCar-Popup form .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500,function(){
          $(this).delay(500).fadeOut(500,function(){
            location.reload();
          })
        })

      },
      error:function(data){
        button_done(button,"استلام");
        alert(data['responseText']);
        var error = data.responseJSON;
        $("#ReciveCar-Popup form label").addClass("alert-danger").fadeIn();
        error_handler(
            error,
            [
              '#KMCounter',
              '#userate',
              '#payrate'


            ],
            [
              'KMCounter',
              'userate',
              'payrate'

            ]
        );
      }
    });
  })
})
