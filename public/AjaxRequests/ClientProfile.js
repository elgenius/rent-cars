$(document).ready(function(){
    $("#Update").submit(function(e){
        var button = $("#Update button")
        button_waiting(button);
        e.preventDefault();
        $.ajax({
            url:"/client/update",
            type:"POST",
            data:$("#Update").serialize(),
            success:function(data){
                                button_done(button,"تعديل");

                PrintOnSelector("#Update .alert","تم تعديل البيانات");
                $("#Update .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500,function(){
                    $(this).delay(500).fadeOut(500,function(){
                        location.reload();
                    })
                })
            },
            error: function (data) {
                button_done(button,"تعديل");
                var error = data.responseJSON;
                $("#Update label").addClass("alert-danger").fadeIn();
                error_handler(
                    error,
                    [   '#client_first',
                        '#client_last',
                        '#client_birthdate',
                        '#client_phone',
                        '#client_address',
                        '#client_national_id',

                    ],
                    [   'first_name',
                        'last_name',
                        'birthdate',
                        'phone',
                        'address',
                        'national_id',
                    ]
                );

            }
        });
    });
    var RentingID ;
    $(".Debts *").click(function(){
        if(this.hasAttribute('data-id'))
        {
            RentingID = $(this).attr('data-id');
            $("#Dept-Popup #IDVal").val(RentingID);
        }
    });
    $("#PayDept").submit(function(e){
        var button = $("#PayDept button")
        button_waiting(button);
        e.preventDefault();
        $.ajax({
            url:"/client/dept/pay",
            type:"POST",
            data:$("#PayDept").serialize(),
            success:function(data){
                button_done(button,"تأكيد");
                if(data==2)
                {
                    PrintOnSelector("#PayDept>.alert","برجاء ادخال المبلغ بشكل صحيح");
                    $("#PayDept .alert").removeClass("alert-success").addClass("alert-danger").fadeIn(500);
                }
                else {
                    PrintOnSelector("#PayDept>.alert", "تم دفع الدين بنجاح");
                    $("#PayDept .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500, function () {
                        $(this).delay(500).fadeOut(500, function () {
                            location.reload();
                        })
                    })
                }
            },
            error: function (data) {
                alert(data['responseText'])
                button_done(button,"تأكيد");
                PrintOnSelector("#PayDept>.alert","حدث خطأ ما . برجاء المحاولة لاحقا");
                $("#PayDept .alert").removeClass("alert-success").addClass("alert-danger").fadeIn(500);
                var error = data.responseJSON;
                $("#PayDept label").addClass("alert-danger").fadeIn();
                error_handler(
                    error,[
                        '#dept'
                    ],
                    [
                        'dept'
                    ]
                );

            }
        });
    });
    $("#AddAttachment-Popup form").submit(function(e){
        var button = $("#AddAttachment-Popup form button");
        button_waiting(button);
        e.preventDefault();
        $.ajax({
            url:"/client/attachment",
            type: 'POST',
            data : new FormData(this),
            contentType: false,
            cache      : false,
            processData: false,
            success:function(data){
                button_done(button,"اضافة");
                PrintOnSelector("#AddAttachment-Popup form .alert","تم الاضافة بنجاح ");
                $("#AddAttachment-Popup form .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500,function(){
                    $(this).delay(500).fadeOut(500,function(){
                        location.reload();
                    })
                })

            },
            error:function(data){
                button_done(button,"اضافة");
                alert(data['responseText']);
                var error = data.responseJSON;
                $("#AddAttachment-Popup form label").addClass("alert-danger").fadeIn();
                error_handler(
                    error,
                    [
                        '#title',
                        '#value'

                    ],
                    [
                        'title',
                        'value'
                    ]
                );
            }
        });
    })
})
