$(document).ready(function(){
    $("#EditEmployee-btn").click(function(){
        var user_id = $(this).siblings("span#user_id").text();
        var id = $(this).parent().siblings("td#id").text();
        var firstname = $(this).siblings("span#firstname").text();
        var lastname = $(this).siblings("span#lastname").text();
        var birthdate = $(this).parent().siblings("td#birthday").text();
        var national_id = $(this).parent().siblings("td#national_id").text();
        var phone = $(this).parent().siblings("td#phone").text();
        var address = $(this).parent().siblings("td#address").text();
        var salary = $(this).parent().siblings("td#salary").text();
        $("#EmployeeInfo-Popup form input#id").val(id);
        $("#EmployeeInfo-Popup form input#user_id").val(user_id);
        $("#EmployeeInfo-Popup form input#first_name").val(firstname);
        $("#EmployeeInfo-Popup form input#last_name").val(lastname);
        $("#EmployeeInfo-Popup form input#birthdate").val(birthdate);
        $("#EmployeeInfo-Popup form input#national_id").val(national_id);
        $("#EmployeeInfo-Popup form input#phone").val(phone);
        $("#EmployeeInfo-Popup form input#address").val(address);
        $("#EmployeeInfo-Popup form input#salary").val(salary);
    })
    $("#EmployeeInfo-Popup form").submit(function(e){
        var button = $("#EmployeeInfo-Popup form button")
        button_waiting(button);

        e.preventDefault();
        $.ajax({
            url:"/employee/update",
            type:"POST",
            data:$("#EmployeeInfo-Popup form").serialize(),
            success:function(data){
                PrintOnSelector("#EmployeeInfo-Popup form .alert","تم تعديل البيانات");
                $("#EmployeeInfo-Popup form .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500,function(){
                    $(this).delay(500).fadeOut(500,function(){
                        location.reload();
                    })
                })
            },
            error: function (data) {
                alert(data['responseText'])
                button_done(button,"تعديل");
                var error = data.responseJSON;
                $("#EmployeeInfo-Popup form label.viewerror").addClass("alert alert-danger").fadeIn();
                error_handler(
                    error,
                    [   '#employee_first',
                        '#employee_last',
                        '#employee_birthdate',
                        '#employee_phone',
                        '#employee_address',
                        '#employee_national_id',
                        '#employee_salary',

                    ],
                    [   'first_name',
                        'last_name',
                        'birthdate',
                        'phone',
                        'address',
                        'national_id',
                        'salary',
                    ]
                );

            }
        });
    })
})
