$(document).ready(function(){
    var CarID;
    $("#Reservations *").click(function(){
        if(this.hasAttribute('data-id'))
        {
            CarID = $(this).attr('data-id');
            $("#DeleteReservation-Popup #IDVal").val(CarID);
        }
    });
    $("#DeleteReservation-Popup form").submit(function(e){
        e.preventDefault();
        $.ajax({
            url:"/reservation/delete",
            type:"POST",
            data:$("#DeleteReservation-Popup form").serialize(),
            success:function(data){
                PrintOnSelector("#DeleteReservation-Popup form .alert","تم الحذف بنجاح");
                $("#DeleteReservation-Popup form .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500,function(){
                    $(this).delay(500).fadeOut(500,function(){
                        location.reload();
                    })
                })

            },
            error:function(data){
                alert(data['responseText']);
                PrintOnSelector("#DeleteReservation-Popup form .alert","حدث خطأ ما . برجاء المحاولة لاحقا");
                $("#DeleteReservation-Popup form .alert").addClass("alert-danger").fadeIn(500,function(){
                    $(this).delay(1000).fadeOut(500,function(){
                        location.reload();
                    })
                })
            }
        });
    })
})
