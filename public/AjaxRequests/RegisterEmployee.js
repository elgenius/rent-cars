$(function () {

    $('#register_employee').submit(function (e) {
        var button = $('#register_employee button');
        button_waiting(button);
        e.preventDefault();
        $.ajax({
            url : '/user/register/Employee',
            type: 'POST',
            data: $('#register_employee').serialize(),
            success: function (data) {
              button_done(button,"اضافة موظف");
                empty_errors( [
                    '#emp_first',
                    '#emp_last',
                    '#emp_birth',
                    '#emp_phone',
                    '#emp_address',
                    '#emp_national_id',
                    '#emp_salary',
                    '#emp_email',
                    '#emp_password',
                    '#emp_conf'
                ]);
                if(data == 1)
                {
                    PrintOnSelector('#register_employee>.alert',"تم اضافة الموظف بنجاح");
                    $("#register_employee>.alert").removeClass("alert-danger").addClass("alert-success").fadeIn(function(){
                        $(this).delay(1000).fadeOut(function(){
                            location.reload();
                        });
                    });
                }else
                {
                     PrintOnSelector('#register_employee>.alert',"حدث خطأ ما برجاء المحاولة لاحقا");
                    $("#register_employee>.alert").removeClass("alert-success").addClass("alert-danger").fadeIn(function(){
                        $(this).delay(1000).fadeOut(function(){
                            location.reload();
                        });

                    })
                }
            },
            error: function (data) {
                // alert(data['responseText']);
                var error = data.responseJSON;
                 button_done(button,"اضافة موظف");
            $("#register_employee label").addClass("alert alert-danger").fadeIn();

                error_handler(
                    error,
                    [   '#emp_first',
                        '#emp_last',
                        '#emp_birth',
                        '#emp_phone',
                        '#emp_address',
                        '#emp_national_id',
                        '#emp_salary',
                        '#emp_email',
                        '#emp_password',
                        '#emp_conf'
                    ],
                    [   'first_name',
                        'last_name',
                        'birthdate',
                        'phone',
                        'address',
                        'national_id',
                        'salary',
                        'email',
                        'password',
                        'password_confirmation']
                );
            }
        });
    });

});