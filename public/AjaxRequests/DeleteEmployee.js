$(document).ready(function(){
    var UserID;
    $("*").click(function(){
        if(this.hasAttribute('data-id'))
        {
            UserID = $(this).attr('data-id');
            $("#DeleteEmployee-Popup #IDVal").val(UserID);
        }
    });
    $("#DeleteEmployee-Popup form").submit(function(e){
        alert(UserID);
        e.preventDefault();
        $.ajax({
            url:"/employee/delete",
            type:"POST",
            data:$("#DeleteEmployee-Popup form").serialize(),
            success:function(data){
                PrintOnSelector("#DeleteEmployee-Popup form .alert","تم الحذف بنجاح");
                $("#DeleteEmployee-Popup form .alert").addClass("alert-success").fadeIn(500,function(){
                    $(this).delay(500).fadeOut(500,function(){
                        location.reload();
                    })
                })

            },
            error:function(data){
                PrintOnSelector("#DeleteEmployee-Popup form .alert","حدث خطأ ما . برجاء المحاولة لاحقا");
                $("#DeleteEmployee-Popup form .alert").addClass("alert-danger").fadeIn(500,function(){
                    $(this).delay(1000).fadeOut(500,function(){
                        location.reload();
                    })
                })
            }
        });
    })
})
