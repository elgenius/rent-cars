$(document).ready(function(){
    var UserID;
    $("#Cars *").click(function(){
        if(this.hasAttribute('data-id'))
        {
            UserID = $(this).attr('data-id');
            $("#DeleteCar-Popup #IDVal").val(UserID);
        }
    });
    $("#DeleteCar-Popup form").submit(function(e){
        e.preventDefault();
        $.ajax({
            url:"/car/delete",
            type:"POST",
            data:$("#DeleteCar-Popup form").serialize(),
            success:function(data){
                PrintOnSelector("#DeleteCar-Popup form .alert","تم الحذف بنجاح");
                $("#DeleteCar-Popup form .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500,function(){
                    $(this).delay(500).fadeOut(500,function(){
                        location.reload();
                    })
                })

            },
            error:function(data){
                alert(data['responseText']);
                PrintOnSelector("#DeleteCar-Popup form .alert","حدث خطأ ما . برجاء المحاولة لاحقا");
                $("#DeleteCar-Popup form .alert").addClass("alert-danger").fadeIn(500,function(){
                    $(this).delay(1000).fadeOut(500,function(){
                        location.reload();
                    })
                })
            }
        });
    })
})
