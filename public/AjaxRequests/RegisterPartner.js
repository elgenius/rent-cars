$(function () {
    $('#partner_register').submit(function (e) {
      var button = $("#partner_register button");
    button_waiting(button);
        e.preventDefault();
        $.ajax({
            url : '/user/register/Partner',
            type: 'POST',
            data: $('#partner_register').serialize(),
            success: function (data) {
              button_done(button,"اضافة شريك");
                empty_errors( [
                    '#partner_first',
                    '#partner_last',
                    '#partner_birth',
                    '#partner_phone',
                    '#partner_address',
                    '#partner_national_id',
                    '#partner_email'
                ]);
                if(data == 1)
                {
                    PrintOnSelector('#partner_register>.alert',"تم اضافة الشريك بنجاح");
                    $("#partner_register>.alert").removeClass("alert-danger").addClass("alert-success").fadeIn(function(){
                        $(this).delay(1000).fadeOut(function(){
                            location.reload();
                        });

                    })

                }else
                {
                    PrintOnSelector('#partner_register>.alert',"حدث خطأ ما برجاء المحاولة لاحقا");
                    $("#partner_register>.alert").removeClass("alert-success").addClass("alert-danger").fadeIn(function(){
                        $(this).delay(1000).fadeOut(function(){
                            location.reload();
                        });

                    })
                }
            },
            error: function (data) {
                button_done(button,"اضافة شريك");
                var error = data.responseJSON;
                $("#partner_register label").addClass("alert alert-danger").fadeIn();
                error_handler(
                    error,
                    [   '#partner_first',
                        '#partner_last',
                        '#partner_birth',
                        '#partner_phone',
                        '#partner_address',
                        '#partner_national_id',
                        '#partner_email'
                    ],
                    [   'first_name',
                        'last_name',
                        'birthdate',
                        'phone',
                        'address',
                        'national_id',
                        'email']
                );

            }
        });
    });
});
