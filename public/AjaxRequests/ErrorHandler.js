/**
 * function to get errors and handle them.
 * array of selectors
 * @param error
 * @param selectors
 * @param fields
 */
function error_handler(error,selectors,fields) {
    var error_text = "";
    for (var i = 0; i < fields.length;i++)
    {
        error_text = error[fields[i]];
        if(error.hasOwnProperty(fields[i]))
        {
            $(selectors[i]).html(error_text);
        }else {
            $(selectors[i]).html('');
        }
    }
}

/**
 * function to empty out the error labels
 * @param selectors
 */
function empty_errors(selectors) {
    for(var i = 0; i < selectors.length ; i++)
    {
        $(selectors[i]).html('');
    }
}

/**
 * function to take selector and print out the text on it
 * @param selector
 * @param text
 * @param type
 */
function PrintOnSelector(selector,text,type) {
    $(selector).html(text);
}
function button_waiting(selector)
{
  selector.html("جاري التنفيذ <i class='fa fa-gear'></i>");
  selector.css({
    background:"#c31925",
    opacity:.7
  });
  selector.prop('disabled', true)
}
function button_done(selector,text)
{
  selector.html(text);
  selector.css({
    background:"#ec1a25",
    opacity:1
  });
  selector.prop('disabled', false)
}
