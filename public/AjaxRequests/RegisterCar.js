$(function () {

    $('#car_register').submit(function(e){
        var button = $("#car_register button[type='submit']");
        button_waiting(button);
        e.preventDefault();
        $.ajax({
            url: '/register/car',
            type: 'POST',
            data : new FormData(this),
            contentType: false,
            cache      : false,
            processData: false,
            success: function (data) {
                button_done(button,"اضافة سيارة");
                empty_errors([
                    '#cars_type',
                    '#cars_model',
                    '#cars_color',
                    '#cars_license_number',
                    '#cars_plate_number',
                    '#cars_motor_number',
                    '#cars_chassis_number',
                    '#cars_price',
                    '#cars_user_id',
                    '#cars_rental_type_id',
                    '#cars_renter_commission'
                ]);
                if(data == 1)
                {
                    PrintOnSelector('#cars_renter_commission',"Car Successfully Created");
                    location.reload();
                }else
                {
                    PrintOnSelector('#cars_renter_commission',"Something went wrong");
                }
            },
            error:function (data) {
                button_done(button,"اضافة سيارة");
                alert(data['responseText']);
                var error = data.responseJSON;
                error_handler(
                    error,
                    [
                        '#cars_type',
                        '#cars_model',
                        '#cars_color',
                        '#cars_license_number',
                        '#cars_plate_number',
                        '#cars_motor_number',
                        '#cars_chassis_number',
                        '#cars_price',
                        '#cars_user_id',
                        '#cars_rental_type_id',
                        '#cars_renter_commission'
                    ],
                    [
                        'type',
                        'model',
                        'color',
                        'license_number',
                        'plate_number',
                        'motor_number',
                        'chassis_number',
                        'price',
                        'user_id',
                        'rental_type_id',
                        'renter_commission'
                    ]
                );
            }
        });
    });
});