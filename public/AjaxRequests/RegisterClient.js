$(function () {
    $('#client_register').submit(function (e) {
        var button = $('#client_register button');
        button_waiting(button);
        e.preventDefault();
        $.ajax({
            url : '/user/register/Customer',
            type: 'POST',
            data: $('#client_register').serialize(),
            success: function (data) {
                                button_done(button,"اضافة عميل");

                empty_errors( [
                    '#client_first',
                    '#client_last',
                    '#client_birth',
                    '#client_phone',
                    '#client_address',
                    '#client_national_id',
                    '#client_email'
                ]);
                if(data == 1)
                {
                    PrintOnSelector('#client_register>.alert',"تم اضافة العميل بنجاح");
                     $("#client_register>.alert").removeClass("alert-danger").addClass("alert-success").fadeIn(function(){
                        $(this).delay(1000).fadeOut(function(){
                            location.reload();
                        });
                     });
                }else
                {
                    PrintOnSelector('#client_register>.alert',"حدث خطأ ما برجاء المحاولة مرة اخري");
                    $("#client_register>.alert").removeClass("alert-success").addClass("alert-danger").fadeIn(function(){
                        $(this).delay(1000).fadeOut(function(){
                            location.reload();
                        });
                         });
                }
            },
            error: function (data) {
                // alert(data['responseText']);
                button_done(button,"اضافة عميل");
                var error = data.responseJSON;
                $("#client_register label").addClass("alert alert-danger").fadeIn();
                error_handler(
                    error,
                    [   '#client_first',
                        '#client_last',
                        '#client_birth',
                        '#client_phone',
                        '#client_address',
                        '#client_national_id',
                        '#client_email'
                    ],
                    [   'first_name',
                        'last_name',
                        'birthdate',
                        'phone',
                        'address',
                        'national_id',
                        'email']
                );
            }
        });
    });
});
