$(document).ready(function(){
  var UserID;
  $("*").click(function(){
    if(this.hasAttribute('data-id'))
    {
       UserID = $(this).attr('data-id');
      $("#DeleteUser-Popup #IDVal").val(UserID);
    }
  });
  $("#DeleteUser-Popup form").submit(function(e){

    e.preventDefault();
    $.ajax({
      url:"/client/delete",
      type:"POST",
      data:$("#DeleteUser-Popup form").serialize(),
      success:function(data){
        alert(data);
        PrintOnSelector("#DeleteUser-Popup form .alert","تم الحذف بنجاح");
        $("#DeleteUser-Popup form .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500,function(){
          $(this).delay(500).fadeOut(500,function(){
            location.reload();
          })
        })

      },
      error:function(data){
        //alert(data['responseText']);
        alert(data);
        PrintOnSelector("#DeleteUser-Popup form .alert","حدث خطأ ما . برجاء المحاولة لاحقا");
        $("#DeleteUser-Popup form .alert").addClass("alert-danger").fadeIn(500,function(){
          $(this).delay(1000).fadeOut(500,function(){
            location.reload();
          })
        })
      }
    });
  })
})
