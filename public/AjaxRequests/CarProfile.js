$(document).ready(function(){
    $("#Update").submit(function(e){
        var button = $("#Update button")
        button_waiting(button);
        e.preventDefault();
        $.ajax({
            url:"/car/update",
            type:"POST",
            data:$("#Update").serialize(),
            success:function(data){
                button_done(button,"تعديل");

                PrintOnSelector("#Update .alert","تم تعديل البيانات");
                $("#Update .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500,function(){
                    $(this).delay(500).fadeOut(500,function(){
                        location.reload();
                    })
                })
            },
            error: function (data) {
                alert(data['responseText'])
                button_done(button,"تعديل");
                var error = data.responseJSON;
                $("#Update label").addClass("alert-danger").fadeIn();
                error_handler(
                    error,
                    [   '#type',
                        '#model',
                        '#color',
                        '#plate_number',
                        '#motor_number',
                        '#chassis_number',
                         '#license_number',
                        '#price'


                    ],
                    [   'type',
                        'model',
                        'color',
                        'plate_number',
                        'motor_number',
                        'chassis_number',
                        'license_number',
                        'price'

                    ]
                );

            }
        });
    })

    $("#AddAttachment-Popup form").submit(function(e){
        var button = $("#AddAttachment-Popup form button");
        button_waiting(button);
        e.preventDefault();
        $.ajax({
            url:"/car/attachment",
            type: 'POST',
            data : new FormData(this),
            contentType: false,
            cache      : false,
            processData: false,
            success:function(data){
                button_done(button,"اضافة");
                PrintOnSelector("#AddAttachment-Popup form .alert","تم الاضافة بنجاح ");
                $("#AddAttachment-Popup form .alert").removeClass("alert-danger").addClass("alert-success").fadeIn(500,function(){
                    $(this).delay(500).fadeOut(500,function(){
                        location.reload();
                    })
                })

            },
            error:function(data){
                button_done(button,"اضافة");
                alert(data['responseText']);
                var error = data.responseJSON;
                $("#AddAttachment-Popup form label").addClass("alert-danger").fadeIn();
                error_handler(
                    error,
                    [
                        '#title',
                        '#value'

                    ],
                    [
                        'title',
                        'value'
                    ]
                );
            }
        });
    })
})
