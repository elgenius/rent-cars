@extends('layouts.Profile')

@section('title')
    {{$user->first_name." ".$user->last_name}}
@endsection
@section('desc')
    العميل : {{$user->first_name." ".$user->last_name}}
@endsection
@section('backto')
    {{URL::route('clients')}}
@endsection
@section('contents')
    <div id="AddAttachment-Popup" class="popup">
        <i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="left" title="اغلاق"></i>
        <!--===== POPUP TITLE -=====-->
        <div class="popup-title">
            <h2>اضافة مرفق جديد</h2>
            <br>
            <hr>
            <hr>
        </div>
        <!--===== POPUP BODY ======-->
        <div class="popup-body text-center">
            <form id="CarAttach" type="POST">
                {!! csrf_field() !!}
                <input type="text" class="" name="id" value="{{$user->id}}">
                <div class="col-md-6 col-xs-12">
                    <input type="text" name="title" placeholder="عنوان المرفق">
                    <label id="general_expense_title"></label>
                </div>
                <div class=" col-md-6 col-xs-12 text-right">
                    <h5 style="float:right;margin:10px 20px;font-size:16px">اضافة صورة</h5>
                    <input type="file" name="picture" id="client_image">
                </div>
                <div class="clearfix"></div>
                <div class="text-center">
                    <button type="submit" class="main-btn">اضافة</button>
                </div>
                <div class="alert"role="alert">

                </div>
            </form>
        </div>
    </div>
    <div id="Dept-Popup" class="popup">
        <i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="left" title="اغلاق"></i>
        <!--===== POPUP TITLE -=====-->
        <div class="popup-title">
            <h2>دفع دين</h2>
            <br>
            <hr>
            <hr>
        </div>
        <!--===== POPUP BODY ======-->
        <div class="popup-body text-center">
            <form id="PayDept" type="POST">
                {!! csrf_field() !!}
                <input type="text" name="id" id="IDVal" class="hidden">
                <div class="col-xs-12">
                <input type="text" name="dept" placeholder="المبلغ الذي سيتم دفعه">
                </div>
                <div class="text-center">
                    <button type="submit" class="main-btn">تأكيد</button>
                </div>
                <div class="alert"role="alert">

                </div>
            </form>
        </div>
    </div>
    <div class="info box main-box">
        <h3 class="title">بيانات العميل</h3>
        <form method="POST" id="Update">
             {!! csrf_field() !!}
            <input class="hidden" name="id" value="{{$user->id}}">
<table border="1">
    
    <tr>
        <th>
            الاسم
        </th>
        <td>
            <div class="col-xs-6">
               <input type="text" name="first_name" value="{{$user->first_name}}">
               <label class="alert" id="client_first"></label>
            </div>
             <div class="col-xs-6">
               <input type="text" name="last_name" value="{{$user->last_name}}">
               <label class="alert" id="client_last"></label>
            </div>
        </td>
    </tr>
    <tr>
        <th>
            الرقم القومي
        </th>
        <td>
             <div class="col-xs-12">
               <input type="text" name="national_id" value="{{$user->national_id}}">
               <label class="alert" id="client_national_id"></label>
            </div>
        </td>
    </tr>
    <tr>
        <th>
            العنوان
        </th>
        <td>
             <div class="col-xs-12">
               <input type="text" name="address" value="{{$user->address}}">
               <label class="alert" id="client_address"></label>
            </div>
        </td>
    </tr>
    <tr>
        <th>
            رقم الموبايل
        </th>
        <td>
             <div class="col-xs-12">
               <input type="text" name="phone" value="{{$user->phone}}">
               <label class="alert" id="client_national_id"></label>
            </div>
        </td>
    </tr>
    <tr>
        <th>
          البريد الالكتروني
        </th>
        <td>
             <div class="col-xs-12">
               <input type="text" name="email" value="{{$user->email}}">
               <label class="alert" id="client_email"></label>
            </div>
        </td>
    </tr>
        <tr>
        <th>
          تاريخ الميلاد
        </th>
        <td>
             <div class="col-xs-12">
               <input type="date" name="birthdate" value="{{ date_format( new DateTime($user->birthdate),"Y-m-d")}}">
               <label class="alert" id="client_birthdate"></label>
            </div>
        </td>
    </tr>
    <tr>
        <th>
            الديون
        </th>
        <td>
            {{$dept}}
        </td>
    </tr>
    <tr>
        <th>
            تقييم الدفع
        </th>
        <td dir="ltr">
            @for ($i = 1; $i <= $payrate; $i++)
                <i class='fa fa-star'></i>
            @endfor
            {{--*/ $payrate = 10 - $payrate /*--}}
            @for($i = 1; $i <= $payrate; $i++)
                <i class='fa fa-star-o'></i>
            @endfor
        </td>
    </tr>
    <tr>
        <th>
            تقييم الاستهلاك
        </th>
        <td dir="ltr">
             @for ($i = 1; $i <= $userate; $i++)
                 <i class='fa fa-star'></i>
                 @endfor
                 {{--*/ $userate = 10 - $userate /*--}}
             @for($i = 1; $i <= $userate; $i++)
                <i class='fa fa-star-o'></i>
                @endfor
        </td>
    </tr>
</table>
<div class="col-xs-12 text-left" style="margin-top:20px">
<button type="submit" class="main-btn sm-btn">تعديل</button>
</div>
</form>
    </div>
<div class="reservations box main-box">
    <h3 class="title">الحجوزات</h3>

    @if(sizeof($rentings)) {{----}}
    <table>
        <tr>
            <th>
                رقم الحجز
            </th>
            <th>
                اسم السيارة
            </th>
            <th>
                من
            </th>
            <th>
                الي
            </th>
            <th>
                المبلغ المطلوب
            </th>
            <th>
                المبلغ المدفوع
            </th>
            <th>
                المبلغ المتبقي
            </th>
            <th>
                تقييم الدفع
            </th>
            <th>
                تقييم الاستهلاك
            </th>
        </tr>
        @foreach($rentings as $renting)
        <tr>
            <td>
{{$renting->id}}
            </td>
            <td>
                {{$renting->car->type}}
            </td>
            <td>
                {{ date_format( new DateTime($renting->start_duration),"Y-m-d")}}
            </td>
            <td>
                {{ date_format( new DateTime($renting->end_duration),"Y-m-d")}}
            </td>
            <td>
                {{$renting->total}}
            </td>
            <td>
                {{$renting->payed}}
            </td>
            <td>

                {{$renting->dept}}

            </td>
            <td dir="ltr">
                {{--*/ $payrate = $renting->payrate /*--}}
                @for ($i = 1; $i <= $payrate; $i++)
                    <i class='fa fa-star'></i>
                @endfor
                {{--*/ $payrate = 10 - $payrate /*--}}
                @for($i = 1; $i <= $payrate; $i++)
                    <i class='fa fa-star-o'></i>
                @endfor
            </td>
            <td dir="ltr">
                {{--*/ $userate = $renting->userate /*--}}
            @for ($i = 1; $i <= $userate; $i++)
                    <i class='fa fa-star'></i>
                @endfor
                {{--*/ $userate = 10 - $userate /*--}}
                @for($i = 1; $i <= $userate; $i++)
                    <i class='fa fa-star-o'></i>
                @endfor
            </td>
        </tr>
            @endforeach
    </table>
        @else
   <h3 class="text-center text-red">لم يتم العثور علي اي حجوزات</h3>
        @endif
</div>
    <div class="Debts box main-box">
        <h3 class="title">الديون</h3>
    @if($dept)
        <table>
            <tr>
                <th>
                    رقم الحجز
                </th>
                <th>
                    الدين
                </th>
                <th></th>
            </tr>
            @foreach($rentings as $renting)
                @if($renting->dept)
            <tr>
                <td>
                    {{$renting->id}}
                </td>

                <td>
                    {{$renting->dept}}
                </td>
                <td><button data-popup="Dept-Popup" data-id="{{$renting->id}}" class="main-btn sm-btn">دفع</button></td>
            </tr>
                @endif
            @endforeach
        </table>
        @else
            <h3 class="text-center text-red">لم يتم العثور علي اي ديون</h3>
        @endif
    </div>
    <div class="attachments box main-box">
        <h3 class="title">الملفات المرفقة</h3>
        <div class="col-xs-12 text-right" style="margin:5px 0">
            <button type="button" class="main-btn" data-popup="AddAttachment-Popup">اضافة مرفق جديد</button>
        </div>
        <table border="1">
            @if(sizeof($user->attachments))
                @foreach($user->attachments as $attachment)
                    <tr>
                        <th>
                            {{$attachment->title}}
                        </th>
                        <td>
                            <img src="{{$attachment->value}}" width="100" height="100">
                            <a style="margin-top:5px;display:block" href="{{$attachment->value}}" download><button class="main-btn sm-btn">تحميل</button></a>

                        </td>
                    </tr>
                @endforeach
            @else
                <h3 class="text-red text-center">لا توجد اي مرفقات بعد </h3>
                <div class="clearfix"></div>
            @endif
        </table>

    </div>
@endsection
@section('script')
    <script>
        $("#client_image").fileinput({
            showUpload: false,
            showCaption: false,
            browseClass: "btn main-btn",
            fileType: "any",
            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
        });
    </script>
<script src="{{ asset('AjaxRequests/ClientProfile.js') }}"></script>
@endsection
