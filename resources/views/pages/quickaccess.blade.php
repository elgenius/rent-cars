@extends('layouts.dashboard')

@section('contents')
<div role="tabpanel" class="tab-pane fade in active" id="QuickAccess">
    <div class="box col-md-6">
        <!--===== POPUP TITLE -=====-->
        <div class="popup-title">
            <h2>اضافة عميل جديد</h2>
            <br>
            <hr>
            <hr>
        </div>
        <!--===== POPUP BODY ======-->
        <div class="popup-body">
            @include('layouts.clients_form')
        </div>
    </div>
    <div class="box col-md-6">
        <!--===== POPUP TITLE -=====-->
        <div class="popup-title">
            <h2>اضافة حجز جديد</h2>
            <br>
            <hr>
            <hr>
        </div>
        <!--===== POPUP BODY ======-->
        <div class="popup-body">
            @include('layouts/reservation_form')
        </div>
    </div>

</div>
@endsection
@section('script')
    <script src="{{ asset('AjaxRequests/RegisterClient.js') }}"></script>
    <script src="{{ asset('AjaxRequests/RegisterReservation.js') }}"></script>
@endsection