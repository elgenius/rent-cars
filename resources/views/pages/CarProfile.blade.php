    @extends('layouts.Profile')

@section('title')
    {{$car->type}}
@endsection
@section('desc')
    السيارة : {{$car->type}}
@endsection
@section('backto')
    {{URL::route('cars')}}
@endsection
@section('contents')
    @include('layouts.Expenses_Forms')
    <div id="AddAttachment-Popup" class="popup">
        <i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="left" title="اغلاق"></i>
        <!--===== POPUP TITLE -=====-->
        <div class="popup-title">
            <h2>اضافة مرفق جديد</h2>
            <br>
            <hr>
            <hr>
        </div>
        <!--===== POPUP BODY ======-->
        <div class="popup-body text-center">
            <form id="CarAttach" type="POST">
                {!! csrf_field() !!}
                <input type="text" class="hidden" name="id" value="{{$car->id}}">
                <input type="text" class="hidden" name="plate_number" value="{{$car->plate_number}}">
                <div class="col-md-6 col-xs-12">
                    <input type="text" name="title" placeholder="عنوان المرفق">
                    <label id="general_expense_title"></label>
                </div>
                <div class=" col-md-6 col-xs-12 text-right">
                    <h5 style="float:right;margin:10px 20px;font-size:16px">اضافة صورة</h5>
                    <input type="file" name="picture" id="car_image">
                </div>
                <div class="clearfix"></div>
                <div class="text-center">
                    <button type="submit" class="main-btn">اضافة</button>
                </div>
                <div class="alert"role="alert">

                </div>
            </form>
        </div>
    </div>
    <div class="info box main-box">
        <h3 class="title">بيانات السيارة</h3>
        <form method="POST" id="Update">
            {!! csrf_field() !!}
            <input class="hidden" name="id" value="{{$car->id}}">
            <table border="1">

                <tr>
                    <th>
                        الاسم
                    </th>
                    <td>
                        <div class="col-xs-12">
                            <input type="text" name="type" value="{{$car->type}}">
                            <label class="alert" id="type"></label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        الموديل
                    </th>
                    <td>
                        <div class="col-xs-12">
                            <input type="text" name="model" value="{{$car->model }}">
                            <label class="alert" id="model"></label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        اللون
                    </th>
                    <td>
                        <div class="col-xs-12">
                            <input type="text" name="color" value="{{$car->color}}">
                            <label class="alert" id="color"></label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        رقم اللوحة
                    </th>
                    <td>
                        <div class="col-xs-12">
                            <input type="text" name="plate_number" value="{{$car->plate_number}}">
                            <label class="alert" id="plate_number"></label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
رقم الموتور
                    </th>
                    <td>
                        <div class="col-xs-12">
                            <input type="text" name="motor_number" value="{{$car->motor_number}}">
                            <label class="alert" id="motor_number"></label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        رقم الشاسيه
                    </th>
                    <td>
                        <div class="col-xs-12">
                            <input type="text" name="chassis_number" value="{{ $car->chassis_number}}">
                            <label class="alert" id="chassis_number"></label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        رقم الرخصة
                    </th>
                    <td>
                        <input type="text" name="license_number" value="{{ $car->license_number}}">
                        <label class="alert" id="license_number"></label>

                    </td>
                </tr>
                 <tr>
                    <th>
                 السعر في اليوم
                    </th>
                    <td>
                        <input type="text" name="price" value="{{ $car->price }}">
                        <label class="alert" id="price"></label>

                    </td>
                </tr>
            </table>
            <div class="col-xs-12 text-left" style="margin-top:20px">
                <button type="submit" class="main-btn sm-btn">تعديل</button>
            </div>
        </form>
    </div>
    <div class="reservations box main-box">
        <h3 class="title">الحجوزات</h3>

        @if(sizeof($rentings))
        <table>
            <tr>
                <th>
                    رقم الحجز
                </th>
                <th>
                    اسم العميل
                </th>
                <th>
                    من
                </th>
                <th>
                    الي
                </th>
                <th>
                    المبلغ المطلوب
                </th>
                <th>
                    المبلغ المدفوع
                </th>
                <th>
                    المبلغ المتبقي
                </th>
            </tr>
            @foreach($rentings as $renting)
            <tr>
                <td>
                    {{$renting->id}}
                </td>
                <td>
                    {{$renting->user->display_name}}
                </td>
                <td>
                    {{$renting->start_duration}}
                </td>
                <td>
                    {{$renting->end_duration}}
                </td>
                <td>
                    {{$renting->total}}
                </td>
                <td>
                    {{$renting->payed}}
                </td>
                <td>
                    {{$renting->dept}}
                </td>
            </tr>
                @endforeach
        </table>
        @else
            <h3 class="text-center text-red">لم يتم العثور علي اي حجوزات</h3>
        @endif
    </div>
    <div class="expenses box main-box">
        <h3 class="title">المصاريف</h3>
        <button data-id="{{$car->id}}" data-popup="AddExpense-Popup" id="AddCarExpense" class="main-btn">اضافة مصروف جديد</button>

    @if(sizeof($car_expenses))
            <table>
                <tr>
                    @foreach($expenses_fields as $field)
                        <th> {{ $field }} </th>
                    @endforeach
                    <th>الخيارات</th>
                </tr>
                @foreach($car_expenses as $car_expense)
                    <tr>
                        <td id="title">
                            {{$car_expense->title}}
                        </td>
                        <td id="value">
                            <div class="col-xs-12">
                                {{$car_expense->value}}
                            </div>
                        </td>
                        <td>
                            <button class="main-btn sm-btn" id="EditExpense-btn" data-popup="ExpensesInfo-Popup">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button class="main-btn sm-btn" data-popup="DeleteExpense-Popup"
                                    data-id="{{$car_expense->id}}">
                                <i class="fa fa-remove"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <h3 class="text-red text-center">لا توجد اي مصاريف بعد </h3>
        @endif
    </div>
    <div class="attachments box main-box">
        <h3 class="title">الملفات المرفقة</h3>
        <div class="col-xs-12 text-right" style="margin:5px 0">
            <button type="button" class="main-btn" data-popup="AddAttachment-Popup">اضافة مرفق جديد</button>
        </div>
        <table border="1">
            @if(sizeof($car->attachments))
            @foreach($car->attachments as $attachment)
            <tr>
                <th>
                   {{$attachment->title}}
                </th>
                <td>
                    <img src="{{$attachment->value}}" width="100" height="100">
                    <a style="margin-top:5px;display:block" href="{{$attachment->value}}" download><button class="main-btn sm-btn">تحميل</button></a>

                </td>
            </tr>
                @endforeach
            @else
                <h3 class="text-red text-center">لا توجد اي مرفقات بعد </h3>
                <div class="clearfix"></div>
            @endif
        </table>

    </div>
@endsection
@section('script')
    <script>
        $("#car_image").fileinput({
            showUpload: false,
            showCaption: false,
            browseClass: "btn main-btn",
            fileType: "any",
            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
        });
    </script>
    <script src="{{ asset('AjaxRequests/CarProfile.js') }}"></script>
    <script src="{{ asset('AjaxRequests/Expenses.js') }}"></script>
    <script src="{{ asset('AjaxRequests/CarExpenses.js') }}"></script>


@endsection