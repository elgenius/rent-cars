@extends('layouts.dashboard')

@section('contents')
    <div id="DeleteReservation-Popup" class="popup">
        <i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="left" title="اغلاق"></i>
        <!--===== POPUP TITLE -=====-->
        <div class="popup-title">
            <h2>حذف الحجز</h2>
            <br>
            <hr>
            <hr>
        </div>
        <!--===== POPUP BODY ======-->
        <div class="popup-body text-center">
            <form id="DeleteReservation" type="POST">
                <h3 class="text-red "> هل انت متأكد بأنك تريد حذف هذا الحجز؟</h3>
                {!! csrf_field() !!}

                <input type="text" class="hidden" name="id" id="IDVal">
                <div class="text-center">
                    <button type="submit" class="main-btn">نعم</button>
                </div>
                <div class="alert"role="alert">

                </div>
            </form>
        </div>
    </div>
    <!-- START ADD Reservation FORM -->
    <div id="AddReservation-Popup" class="popup">
        <i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="left" title="اغلاق"></i>
        <!--===== POPUP TITLE -=====-->
        <div class="popup-title">
            <h2>اضافة حجز جديد</h2>
            <br>
            <hr>
            <hr>
        </div>
        <!--===== POPUP BODY ======-->
        <div class="popup-body">
            @include('layouts/reservation_form')
        </div>
    </div>
    <!-- END ADD Reservation FORM -->

<div role="tabpanel" class="tab-pane fade in active" id="Reservations">
    <button data-popup="AddReservation-Popup" class="main-btn col-xs-3">اضافة حجز</button>
    <form class="col-xs-9">
        <div class="col-md-10 col-xs-9" id="Reservations-Filter">

        </div>
        <div class="col-md-2 col-xs-3" id="Reservations-Length">

        </div>
    </form>
    <div class="clearfix"></div>
    <div class="reservations box main-box">
        <table id="Reservations-table" class="list-view">
            <thead>
            <tr>
                @foreach($rentings_fields as $field)
                    <th>{{ $field }}</th>
                @endforeach
                <th>الحالة</th>
                <th>الخيارات</th>
            </tr>
            </thead>
            <tbody>


            @foreach($rentings as $renting)
                <tr>
                    <td>
                        {{$renting->id}}
                    </td>
                    <td>
                        @if($renting->user)
                            {{ $renting->user->display_name }}
                            @else
                            العميل محذوف
                            @endif


                    </td>
                    <td>
                        @if($renting->car)
                        {{$renting->car->type}}
                            @else
    السيارة محذوفة
                            @endif
                    </td>
                    <td>
                        {{ date_format( new DateTime($renting->start_duration),"Y-m-d")}}
                    </td>
                    <td>
                        {{ date_format( new DateTime($renting->end_duration),"Y-m-d")}}
                    </td>
                    <td>
                        {{$renting->total}}
                    </td>
                    <td>
                        {{$renting->payed}}
                    </td>
                    <td>
                        {{$renting->dept}}
                    </td>
                    <td>
                        @if($renting->deleted_at == NULL)
                            <span style="padding:5px;" class="btn-primary btn-sm">جديد</span>
                        @else
                            <span style="padding:5px;" class="btn-danger btn-sm">منتهي</span>
                            @endif
                    </td>

                    <td>
                        @if($renting->deleted_at == NULL)
                        {{--<button class="main-btn sm-btn" id="EditReservation-btn" data-popup="EditReservation-Popup" data-id="{{ $renting->id }}"><i class="fa fa-pencil"></i></button>--}}
                        <button class="main-btn sm-btn" id="DeleteReservation-btn" data-popup="DeleteReservation-Popup" data-id="{{ $renting->id }}"><i class="fa fa-remove"></i></button>
                        @endif
                    </td>

                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>

@endsection
@section('script')
    <script src="{{ asset('AjaxRequests/RegisterReservation.js') }}"></script>
    <script src="{{ asset('AjaxRequests/DeleteReservation.js') }}"></script>
    <script>
        $("#Reservations-table_filter input").attr("placeholder","بحث عن حجز ؟");
    </script>
@endsection