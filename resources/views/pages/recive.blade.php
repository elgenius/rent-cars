@extends('layouts.dashboard')

@section('contents')
    <div id="ReciveCar-Popup" class="popup">
        <i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="left" title="اغلاق"></i>
        <!--===== POPUP TITLE -=====-->
        <div class="popup-title">
            <h2>استلام السيارة</h2>
            <br>
            <hr>
            <hr>
        </div>
        <!--===== POPUP BODY ======-->
        <div class="popup-body text-center">
            <form id="" type="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input class="hidden" name="id" id="IDVal">
                <input class="hidden" name="plate_number" value="123" id="">
                <div class="col-xs-12">
                    <input type="text" name="KMCounter" placeholder="عداد الكيلومترات">
                    <label for="" id="KMCounter"></label>
                </div>
                <div class="col-xs-12 text-right">
                    <h5 style="float:right;margin:10px 20px;font-size:16px">اضافة صورة</h5>
                    <input type="file" name="picture" id="car_image">
                </div>
                <div class="col-xs-12">
                    <textarea type="text" name="notes" rows="5" placeholder="ملاحظات"></textarea> {{-- make it optional --}}

                </div>
                <div class="col-xs-6">
                    <input type="number" name="userate" placeholder="تقييم الاستهلاك (/10)">
                    <label for="" id="userate"></label>
                </div>
                <div class="col-xs-6">
                    <input type="number" name="payrate" placeholder="تقييم الاستهلاك (/10)">
                    <label for="" id="payrate"></label>
                </div>
                <div class="text-center">
                    <button type="submit" class="main-btn">استلام</button>
                </div>
                <div class="alert">

                </div>
            </form>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade in active" id="Recive">
        <section class="reservations col-xs-12">
            @foreach($rentings as $renting)
                @if($renting->deleted_at==NULL)
            <div class=" item col-md-3" data-popup="ReciveCar-Popup" data-id="{{$renting->id}}">
                <div class="image">
                    <img src="{{$renting->car->picture}}">
                </div>
                <div class="text text-center">
               <h4 class="text-red">{{$renting->car->type}}</h4>
                    <h5>{{$renting->user->display_name}}</h5>
                    <h6>{{date_format( new DateTime($renting->end_duration),"Y-m-d")}}</h6>
                </div>
            </div>
                @endif
            @endforeach
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ asset('AjaxRequests/ReciveCar.js') }}"></script>
@endsection