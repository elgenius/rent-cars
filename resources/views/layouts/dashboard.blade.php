<!doctype html>
<html>

<head>
    <!-- Css Files -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fileinput.min.css') }}">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">


    <!-- Css Files -->
    <meta charset="utf-8">
    <title>لوحة التحكم</title>
</head>


<body>
<div class="background">

</div>

<!-- START ADD Reservation FORM -->
<div id="DeleteUser-Popup" class="popup">
    <i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="left" title="اغلاق"></i>
    <!--===== POPUP TITLE -=====-->
    <div class="popup-title">
        <h2>حذف المستخدم</h2>
        <br>
        <hr>
        <hr>
    </div>
    <!--===== POPUP BODY ======-->
    <div class="popup-body text-center">
        <form id="DeleteUser" type="POST">
          <h3 class="text-red "> هل انت متأكد بأنك تريد حذف هذا المستخدم؟</h3>
            {!! csrf_field() !!}

            <input type="text" class="hidden" name="id" id="IDVal">
            <div class="text-center">
                <button type="submit" class="main-btn">نعم</button>
            </div>
            <div class="alert"role="alert">
              
            </div>
        </form>
    </div>
</div>
<!-- END ADD Reservation FORM -->
<div id="DeleteEmployee-Popup" class="popup">
    <i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="left" title="اغلاق"></i>
    <!--===== POPUP TITLE -=====-->
    <div class="popup-title">
        <h2>حذف المستخدم</h2>
        <br>
        <hr>
        <hr>
    </div>
    <!--===== POPUP BODY ======-->
    <div class="popup-body text-center">
        <form id="DeleteEmployee" type="POST">
          <h3 class="text-red "> هل انت متأكد بأنك تريد حذف هذا المستخدم؟</h3>
            {!! csrf_field() !!}

            <input type="text" class="hidden" name="id" id="IDVal">
            <div class="text-center">
                <button type="submit" class="main-btn">نعم</button>
            </div>
            <div class="alert"role="alert">
              
            </div>
        </form>
    </div>
</div>

<!-- START ADD Reservation FORM -->
<div id="AccountInfo-Popup" class="popup">
    <i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="left" title="اغلاق"></i>
    <!--===== POPUP TITLE -=====-->
    <div class="popup-title">
        <h2>بيانات الحساب</h2>
        <br>
        <hr>
        <hr>
    </div>
    <!--===== POPUP BODY ======-->
    <div class="popup-body">
        <form id="EditInfo">
            {!! csrf_field() !!}
            <input type="text" name="id" value="{{$user->id}}" class="hidden">
            <div class="col-md-4 col-xs-12">
                <label>الاسم الاول</label>
                <input type="text" name="first_name" value="{{ $user->first_name }}">
                <label class="viewerror" id="user_first"></label>
            </div>
            <div class="col-md-4 col-xs-12">
                <label>الاسم الاخير</label>
                <input type="text" name="last_name"  value="{{ $user->last_name }}">
                <label id="user_last"></label>

            </div>
            <div class="col-md-4 col-xs-12">
                <label>تاريخ الميلاد</label>
                <input type="date" name="birthdate"  value="{{ date_format( new DateTime($user->birthdate),"Y-m-d")}}">
                <label class="viewerror" id="user_birthdate"></label>

            </div>
            <div class="col-md-6 col-xs-12">
                <label>رقم التليفون</label>
                <input type="text" name="phone"  value="{{ $user->phone }}">
                <label class="viewerror" id="user_phone"></label>

            </div>
            <div class="col-md-6 col-xs-12">
                <label>رقم البطاقة</label>
                <input type="text" name="national_id"  value="{{ $user->national_id }}">
                <label class="viewerror" id="user_national_id"></label>
            </div>
            <div class="col-xs-12">
                <label>العنوان</label>
                <input type="text" name="address"  value="{{ $user->address }}">
                <label class="viewerror" id="user_address"></label>
            </div>
            <div class="col-md-6 col-xs-12">
                <label>كلمة المرور</label>
                <input name="password" type="password">
                <label class="viewerror" id="user_password"></label>

            </div>
            <div class="col-md-6 col-xs-12">
                <label>تأكيد كلمة المرور</label>
                <input name="password_confirmation" type="password">
                <label class="viewerror" id="user_password_confirm"></label>

            </div>

            <div class="clearfix">
            </div>
            <div class="text-center">
                <button type="submit" class="main-btn">تعديل</button>
            </div>
            <div class="alert"></div>

        </form>
    </div>
</div>
<!-- END ADD Reservation FORM -->

<div id="wrap">
    {{-- header loads here--}}
    @include('layouts.header')
    <div class="wrapper">
        {{-- side panel loads herer--}}
        @include('layouts.sidebar')
        <section class="content col-xs-12">
            <div class="container-fluid">
                <div class="tab-content">


                    @yield('contents')


                </div>
            </div>
        </section>
    </div>
</div>

<div class="clearfix">

</div>
<footer>
    <div class="container">
        <h5 class="fl-right">2016 | جميع الحقوق محفوظة</h5>
        <h5 class="fl-left">تصميم و تطوير <img src="{{ asset('images/aptware.png')}}" width="35"></h5>
    </div>
</footer>
<!-- Js Files -->

<script src="{{ asset('js/jquery-3.1.0.min.js')}}"></script>
<script src="{{ asset('js/bootstrap.js')}}"></script>
<script src="{{ asset('js/jquery.nicescroll.min.js')}}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="{{ asset('AjaxRequests/ErrorHandler.js') }}"></script>
<script src="{{ asset('js/fileinput.min.js') }}"></script>
<script src="{{ asset('AjaxRequests/CarOwnerInstantSearch.js') }}"></script>
<script src="{{ asset('AjaxRequests/UpdateUser.js') }}"></script>
<script src="{{ asset('AjaxRequests/AddGeneralExpense.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/main.js')}}"></script>

<script>
    $('.list-view').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": false,
        "autoWidth": false,
        "language": {
            "zeroRecords":"للاسف لا توجد اي بيانات",
            "oPaginate": {
                "sFirst": "الاولي", // This is the link to the first page
                "sPrevious": "السابق", // This is the link to the previous page
                "sNext": "التالي", // This is the link to the next page
                "sLast": "الاخيرة" // This is the link to the last page
            }
        }
    });
</script>

@yield('script')
<script>
    $("#car_image").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn main-btn",
        fileType: "any",
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
    });
</script>

<script>
    $(".dataTables_filter input").removeClass("form-control input-sm").
    appendTo("#Clients-Filter,#Employees-Filter,#Cars-Filter,#Reservations-Filter,#Partners-Filter");
    $(".dataTables_length select").removeClass("form-control input-sm").
    appendTo("#Clients-Length,#Employees-Length,#Cars-Length,#Reservations-Length,#Partners-Length");
    $("div.dataTables_length label , div.dataTables_filter label").remove();

</script>
<script>$('input[type=date]').datepicker({
        // Consistent format with the HTML5 picker
        dateFormat: 'yy-mm-dd'
    });
</script>

<!-- Js Files -->
</body>

</html>
