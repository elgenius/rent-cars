<header>
    <div class="container">
        <h4 class="fl-right">
            لوحة التحكم
        </h4>
        <i class="fa fa-bars"></i>
        <i class="fa fa-close"></i>

        <li class="dropdown fl-left list-unstyled">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false">{{ $user->first_name." ".$user->last_name }}<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#" data-popup="AccountInfo-Popup"><i class="fa fa-info"></i>بيانات الحساب</a></li>
                <li><a href="{{ URL::route('logout') }}"><i class="fa fa-sign-out"></i>تسحيل الخروج</a></li>

            </ul>
        </li>
    </div>
</header>
