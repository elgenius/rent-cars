<section class="SideBar">
    <!-- Small Nav tabs -->
    <ul class="nav nav-tabs sm" role="tablist">

        @if(Gate::allows('admin',$role) || Gate::allows('partner',$role))
            <li role="presentation"
                @if($page == "overview")
                class="active"
                    @endif
            >
                {{--<a href="#Overview" aria-controls="home" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('overview') }}" aria-controls="home" >
                    <i class="fa fa-globe"></i>
                </a>
            </li>
        @endif

        @if(Gate::allows('employee',$role))
            <li role="presentation"
                @if($page == "quickaccess")
                class="active"
                    @endif
            >
                {{--<a href="#QuickAccess" aria-controls="home" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('quickaccess') }}" aria-controls="home" >
                    <i class="fa fa-globe"></i>
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role) || Gate::allows('employee',$role) )
            <li role="presentation"
                @if($page == "client")
                class="active"
                    @endif
            >
                {{--<a href="#Clients" aria-controls="profile" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('clients') }}" aria-controls="profile" >
                    <i class="fa fa-users"></i>
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role))
            <li role="presentation"
                @if($page == "employee")
                class="active"
                    @endif
            >
                {{--<a href="#Employees" aria-controls="messages" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('employees') }}" aria-controls="messages" >
                    <i class="fa fa-user"></i>
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role))
            <li role="presentation"
                @if($page == "partner")
                class="active"
                    @endif
            >
                {{--<a href="#Partners" aria-controls="settings" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('partners') }}" aria-controls="settings" >
                    <i class="fa fa-exchange"></i>
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role) || Gate::allows('partner',$role))
            <li role="presentation"
                @if($page == "car")
                class="active"
                    @endif
            >
                {{--<a href="#Cars" aria-controls="settings" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('cars') }}" aria-controls="settings" >
                    <i class="fa fa-car"></i>
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role) || Gate::allows('partner',$role) || Gate::allows('employee',$role))
            <li role="presentation"
                @if($page == "reservation")
                class="active"
                    @endif
            >
                {{--<a href="#Reservations" aria-controls="settings" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('reservations') }}" aria-controls="settings" >
                    <i class="fa fa-gear"></i>
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role))
            <li role="presentation"
                @if($page == "report")
                class="active"
                    @endif
            >
                {{--<a href="#Reports" aria-controls="settings" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('reports') }}" aria-controls="settings">
                    <i class="fa fa-file-text-o"></i>
                </a>
            </li>

        @endif

            @if(Gate::allows('admin',$role))
                <li role="presentation"
                    @if($page == "expenses")
                    class="active"
                        @endif
                >
                    {{--<a href="#Reservations" aria-controls="settings" role="tab" data-toggle="tab">--}}
                    <a href="{{ URL::route('expenses') }}" aria-controls="settings" >
                        <i class="fa fa-gear"></i>
                    </a>
                </li>
            @endif
    </ul>
    <!-- Tab panes -->
    <!-- Default Nav tabs -->

    {{-------------------------------------------------------------------------------------------------------------}}
    <ul class="nav nav-tabs default" role="tablist">
        @if(Gate::allows('admin',$role) || Gate::allows('partner',$role))
            <li role="presentation"
                @if($page == "overview")
                class="active"
                    @endif
            >
                {{--<a href="#Overview" aria-controls="home" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('overview') }}" aria-controls="home" >
                    <i class="fa fa-globe"></i>نظرة عامة
                </a>
            </li>
        @endif

        @if(Gate::allows('employee',$role))
            <li role="presentation"
                @if($page == "quickaccess")
                class="active"
                    @endif
            >
                {{--<a href="#QuickAccess" aria-controls="home" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('quickaccess') }}" aria-controls="home" >
                    <i class="fa fa-globe"></i>الوصول السريع
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role) || Gate::allows('employee',$role) )
            <li role="presentation"
                @if($page == "client")
                class="active"
                    @endif
            >
                {{--<a href="#Clients" aria-controls="profile" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('clients') }}" aria-controls="profile" >
                    <i class="fa fa-users"></i>العملاء
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role))
            <li role="presentation"
                @if($page == "employee")
                class="active"
                    @endif
            >
                {{--<a href="#Employees" aria-controls="messages" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('employees') }}" aria-controls="messages" >
                    <i class="fa fa-user"></i>الموظفين
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role))
            <li role="presentation"
                @if($page == "partner")
                class="active"
                    @endif
            >
                {{--<a href="#Partners" aria-controls="settings" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('partners') }}" aria-controls="settings" >
                    <i class="fa fa-exchange"></i>الشركاء
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role) || Gate::allows('partner',$role))
            <li role="presentation"
                @if($page == "cars")
                class="active"
                    @endif
            >
                {{--<a href="#Cars" aria-controls="settings" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('cars') }}" aria-controls="settings" >
                    <i class="fa fa-car"></i>السيارات
                </a>
            </li>
        @endif

        @if(Gate::allows('admin',$role) || Gate::allows('partner',$role) || Gate::allows('employee',$role))
            <li role="presentation"
                @if($page == "reservations")
                class="active"
                    @endif
            >
                {{--<a href="#Reservations" aria-controls="settings" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('reservations') }}" aria-controls="settings" >
                    <i class="fa fa-gear"></i>الحجوزات
                </a>
            </li>
        @endif
            @if(Gate::allows('admin',$role) || Gate::allows('employee',$role))
                <li role="presentation"
                    @if($page == "recive")
                    class="active"
                        @endif
                >
                    {{--<a href="#Reservations" aria-controls="settings" role="tab" data-toggle="tab">--}}
                    <a href="{{ URL::route('recive') }}" aria-controls="settings" >
                        <i class="fa fa-gear"></i>الاستلام
                    </a>
                </li>
            @endif
            @if(Gate::allows('admin',$role))
                <li role="presentation"
                    @if($page == "expenses")
                    class="active"
                        @endif
                >
                    {{--<a href="#Reservations" aria-controls="settings" role="tab" data-toggle="tab">--}}
                    <a href="{{ URL::route('expenses') }}" aria-controls="settings" >
                        <i class="fa fa-gear"></i>المصاريف
                    </a>
                </li>
            @endif
        @if(Gate::allows('admin',$role))
            <li role="presentation"
                @if($page == "reports")
                class="active"
                    @endif
            >
                {{--<a href="#Reports" aria-controls="settings" role="tab" data-toggle="tab">--}}
                <a href="{{ URL::route('reports') }}" aria-controls="settings" >
                    <i class="fa fa-file-text-o"></i>التقارير
                </a>
            </li>
        @endif


    </ul>
    <!-- Tab panes -->
</section>