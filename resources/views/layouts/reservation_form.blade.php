<form id="AddReservation" >
    {!! csrf_field() !!}
    <div class="col-xs-12 ">
        <select name="user_id" type="text" placeholder="اسم العميل">
            <option value="">اختار العميل</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}">{{ $user->display_name." ".$user->phone }}</option>
            @endforeach
        </select>
        <label id="reservation_user_id"></label>
    </div>
    <div class="col-xs-12 ">
        <select name="car_id" type="text" placeholder="اسم السيارة">
            <option value="">اختار السيارة</option>
            @foreach($cars as $car)
                <option value="{{ $car->id }}">{{ $car->display_name }}</option>
            @endforeach
        </select>
        <label id="reservation_car_id"></label>
    </div>
    <div class="col-md-6 col-xs-12">
        <label style="margin-right:15px;">من</label>
        <input name="start_duration" type="date" placeholder="من">
        <label id="reservation_start_duration"></label>
    </div>
    <div class="col-md-6 col-xs-12">
        <label style="margin-right:15px;">الي</label>
        <input name="end_duration" type="date" placeholder="الي">
        <label id="reservation_end_duration"></label>
    </div>
    {{--<div class="col-md-6 col-xs-12">--}}
    {{--<input name="type" id="Day" type="radio">--}}
    {{--<label for="Day" style="margin-right:15px;font-size:16px;" class="text-red">الحجز بنظام اليوم</label>--}}
    {{--</div>--}}
    {{--<div class="col-md-6 col-xs-12">--}}
    {{--<input name="type" id="Month" type="radio">--}}
    {{--<label for="Month" style="margin-right:15px;font-size:16px;" class="text-red">الحجز بنظام الشهر</label>--}}
    {{--</div>--}}
    <div class="col-md-6 col-xs-12">
        <input name="DiscountOption" value="1" id="withdiscount" type="radio">
        <label for="withdiscount" style="margin-right:15px;font-size:16px;" class="text-red">خصم</label>
        <label id="reservation_DiscountOption"></label>
    </div>
    <div class="col-md-6 col-xs-12">
        <input name="DiscountOption" value="0" id="withoutdiscount" type="radio">
        <label for="withoutdiscount" style="margin-right:15px;font-size:16px;" class="text-red">بدون خصم</label>
    </div>
    <div class="col-xs-12" id="discountinput">
        <input name="discount" type="text" placeholder="قيمة الخصم">
        <label id="reservation_discount"></label>
    </div>

    <div class="col-md-6 col-xs-12">
        <label style="margin-right:15px;">المبلغ المطلوب</label>
        <h2 id="RequireMoney" class="text-red" style="margin:0;margin-right:50px;">
            190
        </h2>
        <input type="hidden" name="reservation_required_money" value="0.00">
    </div>

    <div class="col-md-6 col-xs-12">
        <label style="margin-right:15px;">المبلغ المدفوع</label>
        <input type="text" name="payed" >
        <label id="reservation_payed"></label>
    </div>
    <div class="clearfix">
    </div>
    <div class="text-center">
        <button type="submit" class="main-btn">اضافة حجز</button>
    </div>
</form>