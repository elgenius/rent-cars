<form id="client_register"> {!! csrf_field() !!}
    <div class="col-md-6 col-xs-12 ">
        <input name="first_name" type="text" placeholder="الاسم الاول">
        <label id="client_first"></label>
    </div>
    <div class="col-md-6 col-xs-12 ">
        <input name="last_name" type="text" placeholder="الاسم التانى">
        <label id="client_last"></label>
    </div>
    <div class="col-md-6 col-xs-12">
        <h5 style="margin:0;margin-right:20px;margin-bottom:5px">تاريخ الميلاد</h5>
        <input name="birthdate" type="date" style="padding:6px">
        <label id="client_birth"></label>
    </div>
    <div class="col-md-6 col-xs-12">
        <h5 style="margin:0;margin-right:20px;margin-bottom:5px;opacity:0">رقم الهاتف</h5>
        <input name="phone" type="text" placeholder="رقم الهاتف">
        <label id="client_phone"></label>
    </div>
    <div class="col-md-12 col-xs-12">
        <input name="address" type="text" placeholder="العنوان">
        <label id="client_address"></label>
    </div>
    <div class="col-md-12 col-xs-12">
        <input name="national_id" type="text" placeholder="رقم البطاقة">
        <label id="client_national_id"></label>
    </div>
    <div class="col-md-12 col-xs-12">
        <input name="email" type="text" placeholder="البريد الالكترونى">
        <label id="client_email"></label>
    </div>
    <div class="clearfix"></div>
    <div class="alert"></div>
    <div class="clearfix"></div>
    <div class="text-center">
        <button type="submit" class="main-btn">اضافة عميل</button>
    </div>
</form>